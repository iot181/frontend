// import * as home1 from '../../assets/image/home/1.png';
// import * as home2 from '../../assets/image/home/2.png';
// import * as home3 from '../../assets/image/home/3.png';
// import * as home4 from '../../assets/image/home/4.png';
// import * as home5 from '../../assets/image/home/5.png';
// import * as home6 from '../../assets/image/home/6.png';
// import * as home7 from '../../assets/image/home/7.png';
// import * as home8 from '../../assets/image/home/8.png';
// import * as home9 from '../../assets/image/home/9.png';
// import * as home0 from '../../assets/image/home/0.png';
const home1 = (<img className="object-fill h-full" src={require('../../assets/image/home/1.png')} alt="1"/>)
const home2 = (<img className="object-fill h-full" src={require('../../assets/image/home/2.png')} alt="2"/>)
const home3 = (<img className="object-fill h-full" src={require('../../assets/image/home/3.png')} alt="3"/>)
const home4 = (<img className="object-fill h-full" src={require('../../assets/image/home/4.png')} alt="4"/>)
const home5 = (<img className="object-fill h-full" src={require('../../assets/image/home/5.png')} alt="5"/>)
const home6 = (<img className="object-fill h-full"  src={require('../../assets/image/home/6.png')} alt="6"/>)
const home7 = (<img className="object-fill h-full" src={require('../../assets/image/home/7.png')} alt="7"/>)
const home8 = (<img className="object-fill h-full"  src={require('../../assets/image/home/8.png')} alt="8"/>)
const home9 = (<img className="object-fill h-full" src={require('../../assets/image/home/9.png')} alt="9"/>)
const home0 = (<img className="object-fill h-full" src={require('../../assets/image/home/0.png')} alt="0"/>)

const rain = (<img className="object-center w-full" src={require('../../assets/image/weather/rain.jpg')} alt="0"/>)
const cloud = (<img className="object-center w-full" src={require('../../assets/image/weather/cloudy.jpg')} alt="0"/>)
const cold = (<img className="object-center w-full" src={require('../../assets/image/weather/cold.jpg')} alt="0"/>)
const sunny = (<img className="object-center w-full" src={require('../../assets/image/weather/sunny.jpg')} alt="0"/>)
const clear = (<img className="object-center w-full" src={require('../../assets/image/weather/clear.jpg')} alt="0"/>)

const livingroom = (<img className="object-fill h-full"  src={require('../../assets/image/room/livingroom.png')} alt="6"/>)
const kitchen = (<img className="object-fill h-full" src={require('../../assets/image/room/kitchen.png')} alt="7"/>)
const bedroom = (<img className="object-fill h-full"  src={require('../../assets/image/room/bedroom.png')} alt="8"/>)
const bathroom = (<img className="object-fill h-full" src={require('../../assets/image/room/bathroom.png')} alt="9"/>)

export const Images = {
    homeImages: [
        home1, home2, home3, home4, home5, home6, home7, home8, home9, home0
    ],
    weatherImages: {
        rain: rain,
        clear: clear,
        cloud: cloud,
        cold: cold,
        sunny: sunny
    },
    rooms: {
        livingroom: livingroom,
        bedroom: bedroom,
        kitchen: kitchen,
        bathroom: bathroom
    }
}