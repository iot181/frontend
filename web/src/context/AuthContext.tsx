import * as React from 'react';
import { createContext, useState, FC, useEffect } from 'react'

interface IUser {
  _id: string;
  username: string;
  phone: string;
  fullName: string;
  createAt: string;
  home: any;
  __v: number
}
interface IAuthContext {
  token: string | null;
  isLogin: boolean | null;
  user: IUser | null;
  setAuthData?: (token: string, user: IUser) => void;
  clearAuthData?: () => void;
  setTokenRefresh?: () => void;
}

const defaultAuthContext: IAuthContext = {
  token: '',
  isLogin: false,
  user: null
};

export const AuthContext = createContext<IAuthContext>(defaultAuthContext);

export const AuthProvider: FC<any> = ({ children }) => {
  const [token, setToken] = useState<string | null>(localStorage.getItem('token'));
  const [isLogin, setIsLogin] = useState<boolean | null>(localStorage.getItem('isLogin') === 'true');
  const [user, setUser] = useState<IUser | null>(localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')!) : null);

  const setTokenRefresh = () => {
    setToken(localStorage.getItem('token'));
    setIsLogin(localStorage.getItem('isLogin') === 'true');
    setUser(localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')!) : null);
  };

  useEffect(() => {
    setToken(localStorage.getItem('token'));
    setIsLogin(localStorage.getItem('isLogin') === 'true');
    setUser(localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')!) : null);
  }, [token]);

  const setAuthData = (token: string, user: any) => {
    localStorage.setItem('token', token);
    localStorage.setItem('isLogin', JSON.stringify(true));
    localStorage.setItem('user', JSON.stringify(user));
    setToken(token);
    setIsLogin(true);
    setUser(user);
  };
  const clearAuthData = () => {
    localStorage.clear();
    setIsLogin(false);
    setToken('');
    setUser(null);
  };

  const contextData = {
    token,
    isLogin,
    user,
    setAuthData,
    clearAuthData,
    setTokenRefresh
  };

  return <AuthContext.Provider value={contextData}>{children}</AuthContext.Provider>;
};
