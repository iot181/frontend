import {HomeListContainerWrapper, MainWrapper} from "../../style/styled";
import {useContext, useEffect, useState} from "react";
import {getDetail, getList} from "../../api/home/request";
import {AuthContext} from "../../context/AuthContext";
import {toast} from "react-toastify";
import {useParams} from "react-router-dom";
import HomeDetailCard from "../atoms/room/HomeDetailCard";
import RoomListContainer from "../atoms/room/RoomListContainer";
import RoomDetailPopUp from "../atoms/room/RoomDetailPopUp";
import EditHomePopUp from "../atoms/room/EditHomePopUp";
import NewRoomPopUp from "../atoms/room/NewRoomPopUp";

export default function Room () {
    const {token } = useContext(AuthContext);
    const { id } = useParams();
    const [home, setHome] = useState(
        {
            name: '',
            owner: '',
            rooms: [],
            _id: '',
            imagesUrl: ''
        }
    );
    const [editOpen, setEditOpen] = useState(false)
    const [newOpen, setNewOpen] = useState(false)
    const [deleteOpen, setDeleteOpen] = useState(false)
    const [room, setRoom] = useState({_id: ''})
    const [deviceInRoom, setDeviceInRoom] = useState([])
    const [openDetail, setOpenDetail] = useState(false)
    const [editRoomOpen, setEditRoomOpen] = useState(false)
    useEffect(() => {
        (async () => {
            const res = await getDetail(token, id!)
            if (!res.success) {
            }
            else {
                setHome(res.data);
                for (const roomDetail of res.data.rooms) {
                    if (roomDetail._id === room._id) {
                        setRoom(roomDetail)
                    }
                }
            }
        })()
    }, [editOpen, newOpen, editRoomOpen, openDetail, deleteOpen])

    return (
        <MainWrapper>
            <div className="flex flex-row bg-white w-[96%] fixed top-[11%] mx-[2%] border-2 border-gray-200 min-h-[85%] px-5 rounded-2xl space-x-5 items-center">
                <HomeDetailCard home={home} setEditOpen={setEditOpen}></HomeDetailCard>
                <RoomListContainer home={home}
                                   setRoom={setRoom}
                                   setDeviceInRoom={setDeviceInRoom}
                                   setOpenDetail={setOpenDetail}
                                   openDetail={openDetail}
                                   setNewOpen={setNewOpen}
                                   deleteOpen={deleteOpen}
                                   setDeleteOpen={setDeleteOpen}
                ></RoomListContainer>
            </div>
            {openDetail ? <RoomDetailPopUp home={home} room={room} setOpenDetail={setOpenDetail} devices={deviceInRoom} editRoomOpen={editRoomOpen} setEditRoomOpen={setEditRoomOpen}/>: <></>}
            {editOpen? <EditHomePopUp setEditOpen={setEditOpen} homeId={home._id}></EditHomePopUp> : <></>}
            {newOpen ? <NewRoomPopUp setNewOpen={setNewOpen} homeId={home._id}></NewRoomPopUp>: <></>}
        </MainWrapper>
    )
}