import MainPage from "../organisms/MainPage";
import {MainWrapper} from "../../style/styled";
import {useContext, useEffect, useState} from "react";
import {getList} from "../../api/home/request";
import {AuthContext} from "../../context/AuthContext";
import {toast} from "react-toastify";
import HomeListContainer from "../atoms/home/HomeListContainer";
import MainHeader from "../organisms/MainHeader";
import {Icons} from "../../utils/icons/icons";
import NewHomeForm from "../atoms/home/NewHomeForm";
import DeleteHomeForm from "../atoms/home/DeleteHomeForm";

export default function Home () {
    const {token } = useContext(AuthContext);
    const [homes, setHomes] = useState([
        {
            name: '',
            owner: '',
            rooms: [],
            id: ''
        }
    ]);
    const [pagination, setPagination] = useState({total: 0, totalPage: 0})
    const [isOpenNew, setIsOpenNew] = useState(false)
    const [isOpenDelete, setIsOpenDelete] = useState('')
    useEffect(() => {
        (async () => {
            const homeList = await getList(token, 1,10,'')
            if (!homeList.success) {

            }
            else {
                setHomes(homeList.data)
                setPagination(homeList.pagination)
            }
        })()
    }, [isOpenDelete, isOpenNew])
    return (
        <MainWrapper>
            <div className="flex flex-col bg-white w-[96%] fixed top-[11%] mx-[2%] border-2 border-gray-200 min-h-[85%] py-4 px-5 rounded-2xl space-y-0">
                <MainHeader buttonText="New home" buttonIcon = {Icons.icon.homeAdd} setIsOpenNewHomeForm={setIsOpenNew}></MainHeader>
                <HomeListContainer homeListData = {homes} setIsOpenDelete={setIsOpenDelete}/>
            </div>
            {isOpenNew? <NewHomeForm setIsOpenNewHomeForm={setIsOpenNew}/> : <></>}
            {(isOpenDelete !== '')? <DeleteHomeForm isOpenDelete = {isOpenDelete} homes={homes} setIsOpenDelete={setIsOpenDelete}/> : <></>}
        </MainWrapper>
    )
}