import {MainWrapper} from "../../style/styled";
import {useContext, useEffect, useState} from "react";
import {getList} from "../../api/home/request";
import {getList as getRoomList} from "../../api/room/request";
import {getList as getDeviceList} from "../../api/device/request";

import {AuthContext} from "../../context/AuthContext";

export default function Status () {
    const {token} = useContext(AuthContext)
    const [seconds, setSeconds] = useState(0);
    const [homes, setHomes] = useState([
        {
            deviceList: []
        }
    ])

    useEffect(() => {
        const timeout = setTimeout(() => {
            (async () => {
                const homeList = await getList(token, 1,10,'')

                const homeData = homeList?.data;

                for (const home of homeData) {
                    const roomList = await getRoomList(token,1,10, home._id)

                    const rooms = roomList?.data
                    const deviceList: any [] =[];

                    for (const room of rooms) {
                        const deviceListResult = await getDeviceList(token,1,10,'', 'Sensor', '', room._id)
                        deviceListResult?.data.map((result : any) => {
                            deviceList.push(result)
                        })
                    }
                    home.deviceList = deviceList
                }

                setHomes(homeData)
            })()
            setSeconds(seconds + 5)
        }, 2500)
        return () => clearTimeout(timeout);
    }, [seconds])

    return (
        <MainWrapper>
            <div className="flex flex-col bg-white min-w-[96%] fixed top-[12%] mx-[2%] items-center border-2 border-gray-200 min-h-[84%] py-5 px-10 rounded-2xl" >
                <div className='flex flex-col justify-between w-full space-y-3 overflow-y-auto px-5 py-8' >
                    {
                        homes.map((home: any) => {
                            return (
                                (home.deviceList?.length !== 0) &&
                                <div className='flex flex-col w-full space-y-5 px-5 py-5 h-[500px] border-gray-200 border-2 rounded-lg' key={home._id}>
                                    <div className='text-lg font-bold underline'>
                                        {home.name}
                                    </div>
                                    <div className='grid grid-cols-3 overflow-x-auto gap-5 '>
                                        {
                                            home?.deviceList?.map((device: any) => {
                                                return (<div className='flex flex-col h-[400px] space-y-5  px-5 py-5'>
                                                    <div className='font-semibold text-lg'>
                                                        {device.deviceName}
                                                    </div>
                                                    <div className='flex flex-col space-y-2'>
                                                        {device.data.map((e: any) => {
                                                            return (<div className='flex flex-col space-y-0 px-5 py-3 h-[140px] border-gray-200 border-2 rounded-lg bg-green-200'>
                                                                <div className='capitalize text-lg font-semibold w-full'>
                                                                    {e.name}
                                                                </div>
                                                                <div className='text-[50px] px-10 py-3 flex flex-row justify-end'>
                                                                    {e.value? (e.value + `${e.name==='temp' ? ' °C': e.name==='humidity' ? '%': ''}`): ''}
                                                                </div>
                                                            </div>)
                                                        })}
                                                    </div>
                                                </div>)
                                            })
                                        }
                                    </div>
                                </div>
                            )
                        })
                    }
                </div>
            </div>
        </MainWrapper>
    )
}