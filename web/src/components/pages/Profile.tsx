import MainPage from "../organisms/MainPage";
import {MainWrapper} from "../../style/styled";
import {useContext, useEffect, useState} from "react";
import {AuthContext} from "../../context/AuthContext";
import {getList} from "../../api/home/request";
import {toast} from "react-toastify";
import MainHeader from "../organisms/MainHeader";
import {Icons} from "../../utils/icons/icons";
import HomeListContainer from "../atoms/home/HomeListContainer";
import NewHomeForm from "../atoms/home/NewHomeForm";
import DeleteHomeForm from "../atoms/home/DeleteHomeForm";
import {getProfile, register as registerUser, updatePassword, updateProfile} from "../../api/account/request";
import {useForm} from "react-hook-form";

export default function Profile () {
    const {token } = useContext(AuthContext);
    const [user, setUser] = useState({
        _id: '',
        username: "",
        phone: "",
        email: '',
        fullName: "",
        home: [],
        createAt: "",
        __v: 0
    })
    const [editPassword,setEditPassword] = useState(false)
    const [editProfile,setEditProfile] = useState(false)
    const {
        register,
        handleSubmit,
        formState: { errors }
    } = useForm();

    const onSubmitEditPassword = async (data: any) => {
        const response = await updatePassword(token, {oldPassword: data.oldPassword, newPassword: data.newPassword});
        if (response.success) {
            toast.success("Update password successfully!")
        } else {
            toast.error(response.message);
        }
        setEditPassword(false)
    };

    const onSubmitEditProfile = async (data: any) => {
        const response = await updateProfile(token, {fullName: data.fullName, phone: data.phone});
        if (response.success) {
            toast.success("Update profile successfully!")
        } else {
            toast.error(response.message);
        }
        setEditProfile(false)
    };

    useEffect(() => {
        (async () => {
            const userProfile = await getProfile(token)
            if (!userProfile.success) {
                toast.error(userProfile.message)
            }
            else {
                setUser(userProfile.data)
            }
        })()
    }, [editProfile])

    return (
        <MainWrapper>
            <div className="flex flex-col bg-white w-[96%] fixed top-[11%] mx-[2%] border-2 border-gray-200 min-h-[85%] py-4 px-5 rounded-2xl space-y-0">
                <div className="flex flex-row space-x-10 py-10">
                    <div className="w-1/3 rounded-md">
                        <img src={"https://media.istockphoto.com/id/1327592449/vector/default-avatar-photo-placeholder-icon-grey-profile-picture-business-man.jpg?s=612x612&w=0&k=20&c=yqoos7g9jmufJhfkbQsk-mdhKEsih6Di4WZ66t_ib7I="}
                            className="rounded-lg"
                        />
                    </div>
                    <div className="w-2/3 flex flex-col justify-between">
                        <div className="flex flex-col space-y-5 items-start p-8">
                            <div className="text-xl font-bold underline">
                                PROFILE
                            </div>
                            <div className="grid grid-cols-3 gap-2 px-10 w-full">
                                <div className="col-span-1 font-semibold"> Full name </div>
                                <div className="col-span-2"> {user.fullName} </div>
                            </div>
                            <div className="grid grid-cols-3 gap-2 px-10 w-full">
                                <div className="col-span-1 font-semibold"> Email </div>
                                <div className="col-span-2"> {user.email} </div>
                            </div>
                            <div className="grid grid-cols-3 gap-2 px-10 w-full">
                                <div className="col-span-1 font-semibold"> Contact number </div>
                                <div className="col-span-2"> {user.phone} </div>
                            </div>
                            <div className="grid grid-cols-3 gap-2 px-10 w-full">
                                <div className="col-span-1 font-semibold"> Register at </div>
                                <div className="col-span-2"> {user.createAt ? new Date(user.createAt).toLocaleString() : '' } </div>
                            </div>
                            <div className="grid grid-cols-3 gap-2 px-10 w-full">
                                <div className="col-span-1 font-semibold"> Account username </div>
                                <div className="col-span-2"> {user.username} </div>
                            </div>
                        </div>
                        <div className="flex flex-row justify-end px-20 space-x-2">
                            <button className="px-5 py-2 bg-blue-500 text-white font-semibold rounded-md hover:ring-blue-200 hover:ring-4" onClick={()=>setEditProfile(true)}> Edit profile</button>
                            <button className="px-5 py-2 bg-blue-500 text-white font-semibold rounded-md hover:ring-blue-200 hover:ring-4" onClick={()=>setEditPassword(true)}> Reset Password</button>
                        </div>
                    </div>
                </div>
            </div>
            {
                editPassword &&
                    <div className="fixed mx-auto my-auto p-3 rounded-lg inset-0 w-1/3 h-2/5 bg-white shadow-lg border-gray-300 border-2">
                        <div className="text-blue-700 p-2 font-bold"> EDIT PASSWORD </div>
                        <hr className="border-blue-600"/>
                        <form className="flex flex-col space-y-2 py-5 px-5" onSubmit={handleSubmit(onSubmitEditPassword)}>
                            <div className="flex flex-col space-y-1">
                                <div> Old password: </div>
                                <div className='mr-2 text-red-500 text-xs'>
                                    <div className='flex items-center border-2 mb-1 py-1 px-3 rounded-lg'>
                                        <input
                                            id='oldPassword'
                                            className=' pl-2 w-full outline-none border-none text-base text-[#757575]'
                                            type='password'
                                            placeholder='Old password'
                                            {...register('oldPassword', { required: true, minLength: 6 })}
                                        />
                                    </div>
                                    {errors?.oldPassword?.type === 'required' && <p>⚠ This field is required!</p>}
                                </div>
                            </div>
                            <div className="flex flex-col space-y-1">
                                <div> New password: </div>
                                <div className='mr-2 text-red-500 text-xs'>
                                    <div className='flex items-center border-2 mb-1 py-1 px-3 rounded-lg'>
                                        <input
                                            id='newPassword'
                                            className=' pl-2 w-full outline-none border-none text-base text-[#757575]'
                                            type='password'
                                            placeholder='New password'
                                            {...register('newPassword', { required: true, minLength: 6 })}
                                        />
                                    </div>
                                    {errors?.newPassword?.type === 'required' && <p>⚠ This field is required!</p>}
                                </div>
                            </div>
                            <div className="flex flex-row justify-end py-1 space-x-2">
                                <button type='button' className='bg-gray-300 py-1.5 px-4 rounded-md hover:bg-gray-400 text-white font-semibold' onClick={()=>setEditPassword(false)}>
                                    Cancel
                                </button>
                                <button type='submit' className='bg-blue-500 py-1.5 px-4 rounded-md hover:bg-blue-700 text-white font-semibold'>
                                    Change
                                </button>
                            </div>
                        </form>
                    </div>
            }
            {
                editProfile &&
                <div className="fixed mx-auto my-auto p-3 rounded-lg inset-0 w-1/3 h-2/5 bg-white shadow-lg border-gray-300 border-2">
                    <div className="text-blue-700 p-2 font-bold"> EDIT PROFILE </div>
                    <hr className="border-blue-600"/>
                    <form className='bg-white rounded-md p-3' onSubmit={handleSubmit(onSubmitEditProfile)}>
                        <div className="flex flex-col space-y-2">
                            <div> Full name: </div>
                            <div className='mr-2 text-red-500 text-xs'>
                                <div className='flex items-center border-2 mb-1 py-1 px-3 rounded-lg'>
                                    <input
                                        id='fullName'
                                        className=' pl-2 w-full outline-none border-none text-base text-[#757575]'
                                        defaultValue={user.fullName}
                                        type='username'
                                        placeholder='Full name'
                                        {...register('fullName', { required: true, minLength: 6 })}
                                    />
                                </div>
                                {errors?.fullName?.type === 'required' && <p>⚠ This field is required!</p>}
                            </div>
                        </div>

                        <div className="flex flex-row space-x-3 mb-1 mr-2 mb-4">
                            <div className="flex flex-col space-y-2 w-1/2">
                                <div> Email: </div>
                                <div className='text-red-500 text-xs'>
                                    <div className='flex items-center border-2 mb-1 py-1 px-3 rounded-lg'>
                                        <input
                                            id='email'
                                            className=' pl-2 w-full outline-none border-none text-base text-[#757575]'
                                            type='username'
                                            defaultValue={user.email}
                                            disabled={true}
                                            placeholder='Email'
                                        />
                                    </div>
                                </div>
                            </div>

                            <div className="flex flex-col space-y-2 w-1/2" >
                                <div> Contact phone: </div>
                                <div className='text-red-500 text-xs'>
                                    <div className='flex items-center border-2 mb-1 py-1 px-3 rounded-lg'>
                                        <input
                                            id='phone'
                                            className=' pl-2 w-full outline-none border-none text-base text-[#757575]'
                                            type='username'
                                            defaultValue={user.phone}
                                            placeholder='Contact phone'
                                            {...register('phone', { required: true, minLength: 6 })}
                                        />
                                    </div>
                                    {errors?.phone?.type === 'required' && <p>⚠ This field is required!</p>}
                                </div>
                            </div>
                        </div>

                        <div className="flex flex-row justify-end py-1 space-x-2">
                            <button type='button' className='bg-gray-300 py-1.5 px-4 rounded-md hover:bg-gray-400 text-white font-semibold' onClick={()=>setEditProfile(false)}>
                                Cancel
                            </button>
                            <button type='submit' className='bg-blue-500 py-1.5 px-4 rounded-md hover:bg-blue-700 text-white font-semibold'>
                                Done
                            </button>
                        </div>

                    </form>
                </div>
            }
        </MainWrapper>
    )
}