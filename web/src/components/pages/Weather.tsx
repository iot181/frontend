import MainPage from "../organisms/MainPage";
import {HomeListContainerWrapper, MainWrapper} from "../../style/styled";
import {useEffect, useState} from "react";
import {getFiveDayWeather, getWeather} from "../../api/weather/request";
import WeatherCard from "../atoms/weather/WeatherCard";
import HomeCard from "../atoms/home/HomeCard";
import MainHeader from "../organisms/MainHeader";
import {Icons} from "../../utils/icons/icons";
import HomeListContainer from "../atoms/home/HomeListContainer";

export default function Weather () {
    const [weather, setWeather] = useState([
        {
            data: [],
            dateTime: "",
            main: {},
            weatherMain: "",
            wind: {}
        }
    ])

    useEffect(() => {
        (async () => {
            const weatherData = await getFiveDayWeather('Hanoi')
            setWeather(weatherData)
        })()
    }, [])
    return (
        <MainWrapper>
            <div className="flex flex-col bg-white w-[96%] fixed top-[11%] mx-[2%] border-2 border-gray-200 min-h-[85%] py-4 px-5 rounded-2xl space-y-0">
                <HomeListContainerWrapper>
                    <div className="flex flex-row rounded-2xl px-4 py-3 space-x-4 h-[500px]">
                        {
                            weather.map((e: any) => {
                                return (
                                    <div className="w-[340px]">
                                        <WeatherCard weather={e}></WeatherCard>
                                    </div>
                                )
                            })
                        }
                    </div>
                </HomeListContainerWrapper>
            </div>
        </MainWrapper>
    )
}