import MainPage from "../organisms/MainPage";
import {MainWrapper} from "../../style/styled";
import {Icons} from "../../utils/icons/icons";
import {Images} from "../../utils/images/images";


const notifications = [
    {id: 1, message: "Nhiệt độ và độ ẩm thật tốt, ra ngoài chơi thôi!", type: "info", home: {name: "Second Home", image: Images.homeImages[0]}, room: "Living Room", sensorName: "Cảm biến độ ẩm", time: new Date()},
    {id: 2, message: "Nhiệt độ và độ ẩm thật tốt, ra ngoài chơi thôi!", type: "warning", home: {name: "Second Home", image: Images.homeImages[0]}, room: "Living Room", sensorName: "Cảm biến độ ẩm", time: new Date()},
    {id: 3, message: "Nhiệt độ và độ ẩm thật tốt, ra ngoài chơi thôi!", type: "danger", home: {name: "Second Home", image: Images.homeImages[0]}, room: "Living Room", sensorName: "Cảm biến độ ẩm", time: new Date()},
    {id: 4, message: "Nhiệt độ và độ ẩm thật tốt, ra ngoài chơi thôi!", type: "info", home: {name: "Second Home", image: Images.homeImages[0]}, room: "Living Room", sensorName: "Cảm biến độ ẩm", time: new Date()},
    {id: 5, message: "Nhiệt độ và độ ẩm thật tốt, ra ngoài chơi thôi!", type: "happy", home: {name: "Second Home", image: Images.homeImages[0]}, room: "Living Room", sensorName: "Cảm biến độ ẩm", time: new Date()},
    {id: 6, message: "Nhiệt độ và độ ẩm thật tốt, ra ngoài chơi thôi!", type: "danger", home: {name: "Second Home", image: Images.homeImages[0]}, room: "Living Room", sensorName: "Cảm biến độ ẩm", time: new Date()},
    {id: 7, message: "Nhiệt độ và độ ẩm thật tốt, ra ngoài chơi thôi!", type: "info", home: {name: "Second Home", image: Images.homeImages[0]}, room: "Living Room", sensorName: "Cảm biến độ ẩm", time: new Date()},
    {id: 8, message: "Nhiệt độ và độ ẩm thật tốt, ra ngoài chơi thôi!", type: "info", home: {name: "Second Home", image: Images.homeImages[0]}, room: "Living Room", sensorName: "Cảm biến độ ẩm", time: new Date()},
    {id: 9, message: "Nhiệt độ và độ ẩm thật tốt, ra ngoài chơi thôi!", type: "warning", home: {name: "Second Home", image: Images.homeImages[0]}, room: "Living Room", sensorName: "Cảm biến độ ẩm", time: new Date()},
    {id: 10, message: "Nhiệt độ và độ ẩm thật tốt, ra ngoài chơi thôi!", type: "info", home: {name: "Second Home", image: Images.homeImages[0]}, room: "Living Room", sensorName: "Cảm biến độ ẩm", time: new Date()},
    {id: 11, message: "Nhiệt độ và độ ẩm thật tốt, ra ngoài chơi thôi!", type: "info", home: {name: "Second Home", image: Images.homeImages[0]}, room: "Living Room", sensorName: "Cảm biến độ ẩm", time: new Date()},
    {id: 12, message: "Nhiệt độ và độ ẩm thật tốt, ra ngoài chơi thôi!", type: "info", home: {name: "Second Home", image: Images.homeImages[0]}, room: "Living Room", sensorName: "Cảm biến độ ẩm", time: new Date()},
    {id: 13, message: "Nhiệt độ và độ ẩm thật tốt, ra ngoài chơi thôi!", type: "warning", home: {name: "Second Home", image: Images.homeImages[0]}, room: "Living Room", sensorName: "Cảm biến độ ẩm", time: new Date()},
    {id: 14, message: "Nhiệt độ và độ ẩm thật tốt, ra ngoài chơi thôi!", type: "danger", home: {name: "Second Home", image: Images.homeImages[0]}, room: "Living Room", sensorName: "Cảm biến độ ẩm", time: new Date()},
    {id: 15, message: "Nhiệt độ và độ ẩm thật tốt, ra ngoài chơi thôi!", type: "warning", home: {name: "Second Home", image: Images.homeImages[0]}, room: "Living Room", sensorName: "Cảm biến độ ẩm", time: new Date()},
]
export default function Notification () {
    return (
        <MainWrapper>
            <div className="flex flex-col bg-white min-w-[96%] fixed top-[12%] mx-[2%] items-center border-2 border-gray-200 min-h-[84%] py-5 px-10 rounded-2xl" >
                <div className='flex flex-col justify-between w-3/4 space-y-3 h-[520px] overflow-y-auto px-5 py-8' >
                    {notifications.map((e: any) => {
                        const bg = e.type === 'happy' ? 'bg-green-200 border-green-400'
                                    : e.type === 'info' ? 'bg-blue-200 border-blue-400'
                                        : e.type === 'warning' ? 'bg-orange-200 border-orange-400'
                                            : 'bg-red-200 border-red-400'
                        return (
                                <div key={e.id} className={`relative ${bg} px-3 py-5 space-y-3 rounded-lg border-2 hover:ring-4 hover:ring-gray-200`}>
                                    <div className='w-full flex flex-row space-x-5'>
                                        <div className='w-[100px] h-[80px] rounded-lg overflow-hidden'>
                                            {e.home.image}
                                        </div>
                                        <div className='w-full flex flex-col justify-between'>
                                            <div className='flex flex-row justify-between items-center'>
                                                <div className='flex flex-row space-x-1'>
                                                    <div className='font-bold'>
                                                        {e.home.name} -
                                                    </div>
                                                    <div className='font-semibold'>
                                                        {e.room}
                                                    </div>
                                                </div>
                                                <div className='text-sm'>
                                                    {e.time.toLocaleString()}
                                                </div>
                                            </div>
                                            <div className='flex flex-row justify-between items-center'>
                                                <div className='flex flex-row space-x-3 items-center'>
                                                    <svg height={40} width={40}>{Icons.icon.sensor}</svg>
                                                    <div className='font-semibold'>
                                                        {e.sensorName} :
                                                    </div>
                                                    <div>
                                                        {e.message}
                                                    </div>
                                                </div>
                                                <div>
                                                    <svg height={35} width={35}>
                                                        {e.type === 'happy' ? Icons.icon.happy
                                                            : e.type === 'info' ? Icons.icon.infor
                                                                : e.type === 'warning' ? Icons.icon.warning
                                                                    : Icons.icon.danger}
                                                    </svg>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <button className="absolute rounded-full bg-red-400 hover:bg-red-600 ring-2 top-[-20px] right-[-8px] px-1 py-1">
                                        <svg height={20} width={20}>{Icons.icon.close}</svg>
                                    </button>
                                </div>
                        )
                    })}
                </div>
            </div>
        </MainWrapper>
    )
}