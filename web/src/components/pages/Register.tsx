import { useContext, useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import { useNavigate } from 'react-router-dom';
import { register as registerUser } from '../../api/account/request';
import './login.css';
import { appendErrors, useForm } from 'react-hook-form';
import {AuthContext} from "../../context/AuthContext";
export default function RegisterPage() {
    const navigate = useNavigate();
    const { setAuthData, clearAuthData } = useContext(AuthContext);
    const [isValidRewrite, setIsValidRewrite] = useState(false)
    const {
        register,
        handleSubmit,
        formState: { errors }
    } = useForm();

    useEffect(() => {
        clearAuthData?.();
    });

    const goToLogin = () => {
        navigate('/login')
    }

    const onSubmit = async (data: any) => {
        setIsValidRewrite(false)
        if (data.password !== data.passwordRewrite) setIsValidRewrite(true)
        const response = await registerUser(data);
        if (response.success) {
            toast.success("Register successfully!")
            navigate('/login');
        } else {
            toast.error(response.message);
        }
    };

    return (
        <div className='h-screen flex items-center login_img_section'>
            <div className='flex flex-col sm:flex w-full items-center'>
                <div
                    className='
                      bg-black
                      opacity-20
                      inset-0
                      z-0'
                ></div>
                <div className='flex w-full lg:w-1/2 justify-center items-center xl:px-20'>
                    <div className='w-full px-10'>
                        <form className='bg-white rounded-md shadow-2xl p-5' onSubmit={handleSubmit(onSubmit)}>
                            <h1 className='text-green-500 font-bold text-2xl mb-1 hover:text-green-600'>REGISTER FOR YOUR HOME!</h1>
                            <p className='text-sm font-normal text-gray-600 mb-4'></p>
                            <hr className='border-1 border-green-500'/>
                            <p className='text-sm font-normal text-gray-600 mb-4'></p>

                            <div className="flex flex-col space-y-2 mb-4">
                                <div> Full name: </div>
                                <div className='mr-2 text-red-500 text-xs'>
                                    <div className='flex items-center border-2 mb-1 py-1 px-3 rounded-lg'>
                                        <input
                                            id='fullName'
                                            className=' pl-2 w-full outline-none border-none text-base text-[#757575]'
                                            type='username'
                                            placeholder='Full name'
                                            {...register('fullName', { required: true, minLength: 6 })}
                                        />
                                    </div>
                                    {errors?.fullName?.type === 'required' && <p>⚠ This field is required!</p>}
                                </div>
                            </div>

                            <div className="flex flex-row space-x-3 mb-1 mr-2 mb-4">
                                <div className="flex flex-col space-y-2 w-1/2">
                                    <div> Email: </div>
                                    <div className='text-red-500 text-xs'>
                                        <div className='flex items-center border-2 mb-1 py-1 px-3 rounded-lg'>
                                            <input
                                                id='email'
                                                className=' pl-2 w-full outline-none border-none text-base text-[#757575]'
                                                type='username'
                                                placeholder='Email'
                                                {...register('email', { required: true, minLength: 6 })}
                                            />
                                        </div>
                                        {errors?.email?.type === 'required' && <p>⚠ This field is required!</p>}
                                    </div>
                                </div>

                                <div className="flex flex-col space-y-2 w-1/2" >
                                    <div> Contact phone: </div>
                                    <div className='text-red-500 text-xs'>
                                        <div className='flex items-center border-2 mb-1 py-1 px-3 rounded-lg'>
                                            <input
                                                id='phone'
                                                className=' pl-2 w-full outline-none border-none text-base text-[#757575]'
                                                type='username'
                                                placeholder='Contact phone'
                                                {...register('phone', { required: true, minLength: 6 })}
                                            />
                                        </div>
                                        {errors?.phone?.type === 'required' && <p>⚠ This field is required!</p>}
                                    </div>
                                </div>
                            </div>

                            <div className="flex flex-col space-y-2 mb-4">
                                <div> Username: </div>
                                <div className='mr-2 text-red-500 text-xs'>
                                    <div className='flex items-center border-2 mb-1 py-1 px-3 rounded-lg'>
                                        <input
                                            id='username'
                                            className=' pl-2 w-full outline-none border-none text-base text-[#757575]'
                                            type='username'
                                            placeholder='User name'
                                            {...register('username', { required: true, minLength: 6 })}
                                        />
                                    </div>
                                    {errors?.username?.type === 'required' && <p>⚠ This field is required!</p>}
                                    {errors?.username?.type === 'minLength' && <p>⚠ Username cannot be less than 6 characters!</p>}
                                </div>
                            </div>

                            <div className='flex flex-row space-x-2 mb-8 mr-2'>
                                <div className="flex flex-col space-y-2 w-1/2">
                                    <div> Password: </div>
                                    <div className=' text-red-500 text-xs'>
                                        <div className='flex items-center border-2 mb-1 py-1 px-3 rounded-lg '>
                                            <input
                                                className='pl-2 w-full outline-none border-none text-base text-[#757575]'
                                                type='password'
                                                id='password'
                                                placeholder='Password'
                                                {...register('password', { required: true })}
                                            />
                                        </div>
                                        {errors?.password?.type === 'required' && <p>⚠ This field is required!</p>}
                                        {errors?.password?.type === 'minLength' && <p>⚠ Password cannot be less than 6 characters!</p>}
                                    </div>
                                </div>
                                <div className="flex flex-col space-y-2 w-1/2">
                                    <div> Rewrite password: </div>
                                    <div className=' text-red-500 text-xs'>
                                        <div className='flex items-center border-2 mb-1 py-1 px-3 rounded-lg '>
                                            <input
                                                className='pl-2 w-full outline-none border-none text-base text-[#757575]'
                                                type='password'
                                                id='passwordRewrite'
                                                placeholder=''
                                                {...register('passwordRewrite', { required: true })}
                                            />
                                        </div>
                                        {errors?.passwordRewrite?.type === 'required' && <p>⚠ This field is required!</p>}
                                        {errors?.passwordRewrite?.type === 'minLength' && <p>⚠ Password cannot be less than 6 characters!</p>}
                                        {isValidRewrite && <p>⚠ Rewrite password again!</p>}
                                    </div>
                                </div>
                            </div>

                            <div className='w-full flex justify-between pr-2'>
                                <span className='text-sm ml-2 hover:text-blue-500 cursor-pointer hover:-translate-y-1 duration-500 transition-all' onClick={goToLogin}>
                                    Have account?
                                </span>
                                <button
                                    type='submit'
                                    className='block w-1/3 bg-green-500 py-2 rounded-md hover:bg-green-700 hover:-translate-y-1 transition-all duration-500 text-white font-semibold mb-2'
                                >
                                    Register
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
}