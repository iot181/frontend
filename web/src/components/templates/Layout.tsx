import Navbar from '../organisms/Navbar';
import {useState} from "react";
import { WrapperAll, WrapperContent, Content} from '../../style/styled';
import {Outlet} from "react-router-dom";
import Footbar from "../organisms/Footbar";
import MainPage from "../organisms/MainPage";

export default function Layout() {
    const [collapsed, setCollapsed] = useState<boolean>(false);
    const handleCollapsedChange = () => {
        setCollapsed(!collapsed);
    };
    return (
        <WrapperAll>
            <div className="flex flex-col">
                <div>
                    <Navbar />
                </div>
                <div>
                    <Outlet/>
                </div>
                <div>
                    <Footbar/>
                </div>
            </div>
        </WrapperAll>
    );
}
