import {FooterWrapper, HeaderWrapper, Image, MainWrapper} from "../../style/styled";
import {Icons} from "../../utils/icons/icons";
import FooterButton from "./FooterButton";

export default function MainPage(outlet: any) {
    return (
        <MainWrapper>
            <div className="flex flex-col bg-white min-w-[96%] fixed top-[12%] mx-[2%] items-center border-2 border-gray-200 min-h-[84%] py-5 px-10 rounded-2xl">
                <div></div>
            </div>
        </MainWrapper>
    )
}
