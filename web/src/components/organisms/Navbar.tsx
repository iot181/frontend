import {HeaderWrapper, Image} from "../../style/styled";
import {useContext} from "react";
import {AuthContext} from "../../context/AuthContext";
import {useNavigate} from "react-router-dom";
import {toast} from "react-toastify";
import {Icons} from "../../utils/icons/icons";

export default function Navbar() {
  const storeName = "SMART HOME";
  const {clearAuthData} = useContext(AuthContext)
  const navigate = useNavigate()
  const logout = () => {
    clearAuthData?.();
    toast.success('Logout successful!');
  }
  const returnHome = () => {
    navigate('/')
  }
  const goToPersonal = () => {
    navigate('/profile')
  }
  const goToNotification = () => {
    navigate('/notification')
  }
  return (
      <HeaderWrapper className="w-full fixed top-0">
        <nav className='fixed border-0 border-b-2 flex top-0 w-screen justify-between px-[40px] py-2 items-center bg-white z-10'>
          <div className='flex items-center'>
            <svg height={42} width={42}>{Icons.icon.home}</svg>
            <div className="ml-3 space-y-0">
              <h1 className='text-lg text-green-600 font-bold'> {storeName}</h1>
              <h1 className ='text-xs text-green-500 font-bold underline'>
                Life is one click away from being better!
              </h1>
            </div>
          </div>
          <div className='flex items-center w-2/3 justify-between'>
            <div className='md:flex items-center border px-2 py-1 border-gray-200 rounded-md bg-white hidden w-1/2'>
              <svg
                  xmlns='http://www.w3.org/2000/svg'
                  className='h-5 w-5 pt-0.5 text-gray-500'
                  fill='none'
                  viewBox='0 0 24 24'
                  stroke='currentColor'
              >
                <path strokeLinecap='round' strokeLinejoin='round' strokeWidth='5' d='M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z' />
              </svg>
              <input className='ml-2 bg-white font- outline-0' type='text' name='search' id='search' placeholder='Search...' />
            </div>
            <ul className='flex items-center space-x-6 ml-6'>
              <li>
                <div className="cursor-pointer">
                  <img
                      src={Icons.icon.avatar as unknown as string}
                      className="rounded-full"
                      style={{
                        width: '35px',
                        height: '35px',
                      }}
                      onClick = {goToPersonal}
                  />
                </div>
              </li>
              <li>
                <div className="cursor-pointer" onClick={goToNotification}>
                  <svg xmlns='http://www.w3.org/2000/svg' className='h-6 w-6 text-black' fill='none' viewBox='0 0 24 24' stroke='currentColor'>
                    <path
                        strokeLinecap='round'
                        strokeLinejoin='round'
                        strokeWidth='2'
                        d='M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9'
                    />
                  </svg>
                </div>
              </li>
              <li>
                <svg
                    xmlns='http://www.w3.org/2000/svg'
                    className='h-6 w-6 text-black'
                    fill='none'
                    viewBox='0 0 24 24'
                    stroke='currentColor'
                    strokeWidth='2'
                    onClick={logout}
                >
                  <path
                      strokeLinecap='round'
                      strokeLinejoin='round'
                      strokeWidth='2'
                      d='M17 16l4-4m0 0l-4-4m4 4H7m6 4v1a3 3 0 01-3 3H6a3 3 0 01-3-3V7a3 3 0 013-3h4a3 3 0 013 3v1'
                  />
                </svg>
              </li>
            </ul>
          </div>
        </nav>
      </HeaderWrapper>
  );
}
