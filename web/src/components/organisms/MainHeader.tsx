import {MainHeaderWrapper} from "../../style/styled";
import {Icons} from "../../utils/icons/icons";

export default function MainHeader(props : any) {
    const {buttonText, buttonIcon, setIsOpenNewHomeForm} = props;
    return (
        <MainHeaderWrapper className = "flex flex-row justify-end items-centered space-x-5 px-1">
            <div className='md:flex items-center border px-2 py-1 border-gray-200 rounded-md bg-white hidden'>
                <svg className="w-5 h-5">{Icons.icon.search}</svg>
                <input className='ml-2 bg-white outline-0' type='text' name='search' id='search' placeholder='Name' />
            </div>
            <button className="flex flex-row bg-green-400 hover:bg-green-600 rounded-lg px-3 py-1.5 text-white font-bold items-center space-x-0" onClick={() => setIsOpenNewHomeForm(true)}>
                <svg className="mr-2 w-4 h-4  ">{buttonIcon}</svg>
                {buttonText}
            </button>
        </MainHeaderWrapper>
    )
}
