import { useState} from "react";
import {useNavigate} from "react-router-dom";

const REACT_APP_FE_HOST = process.env.REACT_APP_FE_HOST;

export default function FooterButton(prop: any) {
    const {text, image, path} = prop;
    const navigate = useNavigate();
    const goToPath = () => {
        navigate(path);
    }
    return (
        <div>
            <button className={`ml:text-lg font-bold border-0 hover:border-b-4 border-green-500 px-20 py-2 rounded-sm 
                ${(window.location.href == REACT_APP_FE_HOST+ path) ? 'border-b-4' : 'border-0'}`}>
                <div className="flex flex-row justify-between items-center" onClick={goToPath}>
                    <svg className="mr-2 w-6 h-6 ">{image}</svg>
                    {text}
                </div>
            </button>
        </div>
    )
}