import {FooterWrapper, HeaderWrapper, Image} from "../../style/styled";
import {useContext} from "react";
import {AuthContext} from "../../context/AuthContext";
import {useNavigate} from "react-router-dom";
import {toast} from "react-toastify";
import {Icons} from "../../utils/icons/icons";
import FooterButton from "./FooterButton";
import {constRoute} from "../../routes/ConstRoute";

export default function Footbar() {
    return (
        <FooterWrapper>
            <div className="bg-grey-500 grid w-[100%] fixed bottom-0 items-center ml:h-24 md:h-16 place-items-center px-10">
                <div className="bg-white flex flex-row items-center ml:h-20 md:h-12 border-2 border-b-green-500 rounded-full px-5">
                    {constRoute.map((e, index) => {
                        return (<FooterButton text={e.title} image={e.icon} path={e.path} key={index}/>)
                    })}
                </div>
            </div>
        </FooterWrapper>
    )
}
