import {HomeListContainerWrapper} from "../../../style/styled";
import HomeCard from "./HomeCard";

export default function HomeListContainer(props: any) {
    const {homeListData, setIsOpenDelete} = props;
    return (
        <HomeListContainerWrapper>
            <div className="flex flex-row rounded-2xl px-4 py-3 space-x-7 h-[500px]">
                {(homeListData) ? homeListData.map((homeData: any, index: number) => {
                    return (<HomeCard setIsOpenDelete={setIsOpenDelete} homeData = {homeData} inListView= {true} homeOrderNumber = {index} key={index}/>)
                }) : (
                    <div className="font-bold text-lg"> You have no any home. Add new home!</div>
                )}
            </div>
        </HomeListContainerWrapper>
    )
}
