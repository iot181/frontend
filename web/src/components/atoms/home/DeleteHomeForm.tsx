import {useContext, useEffect, useState} from "react";
import {Icons} from "../../../utils/icons/icons";
import {createHome, deleteDetail} from "../../../api/home/request";
import {AuthContext} from "../../../context/AuthContext";
import {toast} from "react-toastify";
import { useForm } from "react-hook-form";
import {useNavigate} from "react-router-dom";

export default function DeleteHomeForm(props: any) {
    const {token} = useContext(AuthContext)
    const {setIsOpenDelete, homes, isOpenDelete} = props;
    const { register, handleSubmit, formState: { errors } } = useForm({
        criteriaMode: "all"
    });

    const [homeName, setHomeName] = useState('')
    useEffect(() => {
        homes.map((home: any) => {
            if (home._id === isOpenDelete) setHomeName(home.name)
        })
    }, [])
    const submitDeleteNewHome = async () => {
        const response = await deleteDetail(token, isOpenDelete)
        if (response.success) {
            setIsOpenDelete('');
            toast.success(response.message);
        } else {
            toast.error(response.message);
        }
    }
    return (
        <div className= "fixed mx-auto my-auto inset-0 h-[350px] w-[500px] border-2 bg-white shadow-gray-300 shadow-xl rounded-xl border-2 border-gray-400">
            <div className="relative flex-1 bg-green-500 text-lg font-bold text-white rounded-t-xl text-center py-2">
                <div> DELETE HOME </div>
                <div className="absolute right-0 top-0 px-2 py-2">
                    <button className="rounded-full bg-red-400 hover:bg-red-600 ring-2 hover:ring-4 px-2 py-2 " onClick={() => setIsOpenDelete('')}>
                        <svg height={15} width={15}>{Icons.icon.close}</svg>
                    </button>
                </div>
            </div>
            <div className="flex flex-col justify-between rounded-lg h-[320px] w-[480px] mx-auto mt-3 px-5 py-2">
                <form onSubmit={handleSubmit(submitDeleteNewHome)} className="flex flex-col space-y-40" >
                    <div className="space-y-2 flex flex-col">
                        <label className="text-gray-700 font-bold">
                            Do you really want to delete this home?
                        </label>
                        <label className="text-gray-700">
                            {homeName}
                        </label>
                    </div>
                    <div className="relative flex flex-col space-y-3">
                        <button
                            className="bg-orange-400 hover:bg-orange-600 text-white font-bold py-2 px-4 rounded-md focus:outline-none focus:shadow-outline"
                            type="submit"
                        >
                            Yes! Delete it!
                        </button>
                    </div>
                </form>
            </div>
        </div>
    )
}
