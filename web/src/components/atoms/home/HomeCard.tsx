import {HomeCardWrapper} from "../../../style/styled";
import {Images} from "../../../utils/images/images";
import {useNavigate} from "react-router-dom";
import {useEffect, useState} from "react";
import {Icons} from "../../../utils/icons/icons";
import {getWeather} from "../../../api/weather/request";

export default function HomeCard(props: any) {
    const {homeData, homeOrderNumber, inListView, setIsOpenDelete} =  props;
    const [weather, setWeather] = useState({
        main: {
            "temp": 300.15,
            "feels_like": 301.76,
            "temp_min": 300.15,
            "temp_max": 300.15,
            "pressure": 1019,
            "humidity": 67,
            "sea_level": 1019,
            "grnd_level": 1017
        },
        weather: [{
            main: '',
            description: "",
        }],
        wind: {
            "speed": 1.43,
            "deg": 317,
            "gust": 1.62
        }
    })
    const navigate = useNavigate()
    const goToRoomList = (homeData) => {
        navigate(`/${homeData._id}`);
    }
    const roomString = (homeData.rooms.length).toString() + ((homeData.rooms.length > 1)? ' rooms': ' room')
    useEffect(() => {
        (async () => {

            const roomAddress = homeData?.address? homeData.address: 'Hanoi';
            const todayWeather = await getWeather(roomAddress)
            setWeather(todayWeather)
        })()
    }, [])
    return (
        <HomeCardWrapper className="relative bg-white hover:shadow-green-400 hover:shadow-lg shadow-md shadow-gray-300 hover:bg-gray-100">
            <div className="relative flex flex-col rounded-lg h-[450px] w-[440px] mx-1 px-4 py-1 space-y-2" onClick={() => {goToRoomList(homeData)}}>
                <div className="h-[240px] overflow-hidden rounded-lg">
                    { (!homeData.imagesUrl)  ? Images.homeImages[homeOrderNumber % (Images.homeImages.length)]
                     : <img className="object-fill h-full" src={homeData.imageUrl} alt={homeData._id}/>}
                </div>
                <div className="h-[2px] bg-gray-200 w-full"></div>
                <div className="flex flex-row justify-between items-center px-4">
                    <div className="text-lg font-bold">{homeData.name}</div>
                    <div className="text-sm font-bold">{roomString} </div>
                </div>
                <div className="flex flex-col space-y-2 border-0 border-t-2 py-2 border-gray-200 px-4 rounded-lg">
                    <div className='text-sm'> Weather: {weather.weather[0].main} ({weather.weather[0].description}) </div>
                    <div className="grid grid-cols-2 gap-x-10 gap-y-1">
                        <div className="col-span-2 text-xs"><b>Temperature:</b> {weather.main?.temp}℃ &nbsp; &nbsp; (min: {weather.main?.temp_min}℃ - max: {weather.main?.temp_max}℃) </div>
                        <div className="text-xs"><b>Air pressure:</b> {weather.main?.pressure}00 Pa</div>
                        <div className="text-xs"><b>Humidity:</b> {weather.main?.humidity}%</div>
                        <div className="text-xs"><b>Wind:</b> {weather.wind?.speed}km/h, {weather.wind?.deg}°</div>
                    </div>
                    <div className="font-bold text-lg">
                </div>
                {
                    inListView ? <button className="absolute bottom-0 right-2 left-2 bg-blue-500 rounded-lg py-2 text-white font-bold hover:bg-blue-700" onClick={() => {goToRoomList(homeData)}}>VIEW DETAIL</button>
                    :<button className="absolute bottom-0 right-2 bg-red-500 rounded-lg py-2 px-5 text-white font-bold hover:bg-red-700" onClick={() => {goToRoomList(homeData)}}>DELETE</button>
                }
            </div>
            <button className="absolute rounded-full bg-red-400 hover:bg-red-600 ring-2  top-2 right-3 px-1 py-1" onClick={()=>setIsOpenDelete(homeData._id)}>
                <svg height={20} width={20}>{Icons.icon.close}</svg>
            </button>
        </div>
    </HomeCardWrapper>
    )
}
