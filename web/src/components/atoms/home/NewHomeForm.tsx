import {useContext, useState} from "react";
import {Icons} from "../../../utils/icons/icons";
import {createHome} from "../../../api/home/request";
import {AuthContext} from "../../../context/AuthContext";
import {toast} from "react-toastify";
import { useForm } from "react-hook-form";
import {useNavigate} from "react-router-dom";

export default function NewHomeForm(props: any) {
    const {token} = useContext(AuthContext)
    const {setIsOpenNewHomeForm} = props;
    const { register, handleSubmit, formState: { errors } } = useForm({
        criteriaMode: "all"
    });

    const submitCreateNewHome = async (name: any) => {
        const response = await createHome(token, name)
        if (response.success) {
            setIsOpenNewHomeForm(false);
            toast.success(response.message);
        } else {
            toast.error(response.message);
        }
    }
    return (
        <div className= "fixed mx-auto my-auto inset-0 h-[350px] w-[500px] border-2 bg-white shadow-gray-300 shadow-xl rounded-xl border-2 border-gray-400">
            <div className="relative flex-1 bg-green-500 text-lg font-bold text-white rounded-t-xl text-center py-2">
                <div> CREATE NEW HOME </div>
                <div className="absolute right-0 top-0 px-2 py-2">
                    <button className="rounded-full bg-red-400 hover:bg-red-600 ring-2 hover:ring-4 px-2 py-2 " onClick={() => setIsOpenNewHomeForm(false)}>
                        <svg height={15} width={15}>{Icons.icon.close}</svg>
                    </button>
                </div>
            </div>
            <div className="flex flex-col justify-between rounded-lg-[320px] w-[480px] mx-auto mt-3 px-5 py-2">
                <form onSubmit={handleSubmit(submitCreateNewHome)} className="flex flex-col space-y-24" >
                    <div className="space-y-2">
                        <label className="text-gray-700 font-bold">
                            Name of new home:
                        </label>
                        <input
                            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                            type="text" placeholder="Name of home" {...register("name", {required: true})}/>
                        {errors.name && <span className="text-red-500 text-sm"> ⚠ This field is required!</span>}
                    </div>
                    <div className="relative flex flex-col space-y-3">
                        <label className="text-gray-700 font-bold flex flex-row space-x-10">
                            Image:
                            <button
                                className="flex flex-row space-x-3 bg-gray-500 hover:bg-gray-700 text-white py-1 px-2 rounded-md focus:outline-none focus:shadow-outline ml-5"
                                type="submit">
                                <svg width={20} height={20}>{Icons.icon.upload}</svg>
                                Upload
                            </button>
                        </label>
                        <button
                            className="bg-orange-400 hover:bg-orange-600 text-white font-bold py-2 px-4 rounded-md focus:outline-none focus:shadow-outline"
                            type="submit"
                        >
                            Create Home!
                        </button>
                    </div>
                </form>
            </div>
        </div>
    )
}
