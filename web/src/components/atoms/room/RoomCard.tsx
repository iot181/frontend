import {HomeCardWrapper} from "../../../style/styled";
import {Images} from "../../../utils/images/images";
import {useNavigate} from "react-router-dom";
import {useContext, useEffect, useState} from "react";
import {getList} from "../../../api/device/request";
import {toast} from "react-toastify";
import {AuthContext} from "../../../context/AuthContext";
import DeviceItemList from "../device/DeviceItemList";
import {Icons} from "../../../utils/icons/icons";
import {getDetail} from "../../../api/room/request";

export default function RoomCard(props: any) {
    const {token} = useContext(AuthContext)
    const {roomData, setOpenDetail, setRoom, setDeviceInRoom, setDeleteOpen, setTargetedRoom, openDetail} =  props

    const [roomDetail, setRoomDetail] = useState({name:'', type:'', devices: []})
    const [devices, setDevices] = useState([])

    const popupRoom = () => {
        setOpenDetail(true);
        setRoom(roomData);
        setDeviceInRoom(devices)
    }

    useEffect(() => {
        (async () => {
            const roomDetailData = await getDetail(token, roomData._id);
            setRoomDetail(roomDetailData.data)

            const res = await getList(token, 1,  100 , "", "", "", roomData._id)
            if (!res.success) {

            }
            else {
                setDevices(res.data)
            }
        })()
    }, [openDetail])

    let commonDeviceCount = 0, sensorCount = 0;
    roomDetail.devices.map((e: any) => {
        if (e.deviceType === 'Common') commonDeviceCount++;
        else if (e.deviceType === 'Sensor') sensorCount++;
    })
    const deviceString = commonDeviceCount + ((commonDeviceCount > 1) ? ' devices' : ' device') + ", " + sensorCount + ((sensorCount > 1) ? ' sensors' : ' sensor');
    return (
        <div className = "relative shadow-lg border-2 bg-gray-200 h-[200px] rounded-lg flex flex-row px-2 py-2 space-x-5 hover:bg-gray-300 hover:shadow-green-300">
            <div className="rounded-lg w-[320px]"  onClick={popupRoom}>
                <div className="overflow-hidden rounded-lg">
                    {
                        roomDetail.type.toLowerCase().includes('bed') ? Images.rooms.bedroom
                            : roomDetail.type.toLowerCase().includes('bath') ? Images.rooms.bathroom
                                : roomDetail.type.toLowerCase().includes('kitchen') ? Images.rooms.kitchen
                                    : Images.rooms.livingroom
                    }
                </div>
            </div>
            <div className="flex flex-col w-[370px] justify-between">
                <div className="flex flex-col"  onClick={() => setOpenDetail(true)}>
                    <div className="flex flex-row items-center justify-between">
                        <div className="text-lg font-bold underline">{roomDetail.name? roomDetail.name: "Unname"}</div>
                        <div className="font-bold">{roomDetail.type? roomDetail.type: "Living room"}</div>
                    </div>
                    <div className="text-sm">{deviceString}</div>
                </div>
                <DeviceItemList devices={devices}></DeviceItemList>
            </div>
            <button className="absolute rounded-full bg-red-400 hover:bg-red-600 ring-2  top-[-10px] right-[-8px] px-1 py-1" onClick={()=>{setDeleteOpen(true); setTargetedRoom(roomData)}}>
                <svg height={20} width={20}>{Icons.icon.close}</svg>
            </button>
        </div>
    )
}
