import {useContext} from "react";
import {Icons} from "../../../utils/icons/icons";
import {AuthContext} from "../../../context/AuthContext";
import {toast} from "react-toastify";
import { useForm } from "react-hook-form";
import {deleteDetail} from "../../../api/room/request";

export default function DeleteRoomPopUp(props: any) {
    const {token} = useContext(AuthContext)
    const {setDeleteOpen, room} = props;
    const { handleSubmit, formState: { errors } } = useForm({
        criteriaMode: "all"
    });

    const submitDeleteRoom = async () => {
        const response = await deleteDetail(token, room._id)
        if (response.success) {
            setDeleteOpen(false);
            toast.success(response.message);
        } else {
            toast.error(response.message);
        }
    }
    return (
        <div className= "fixed mx-auto my-auto inset-0 h-[250px] w-[500px] border-2 bg-white shadow-gray-300 shadow-xl rounded-xl border-2 border-gray-400 z-index-2">
            <div className="relative flex-1 bg-green-500 text-lg font-bold text-white rounded-t-xl text-center py-2">
                <div> DELETE ROOM </div>
                <div className="absolute right-0 top-0 px-2 py-2">
                    <button className="rounded-full bg-red-400 hover:bg-red-600 ring-2 hover:ring-4 px-2 py-2 " onClick={() => setDeleteOpen(false)}>
                        <svg height={15} width={15}>{Icons.icon.close}</svg>
                    </button>
                </div>
            </div>
            <div className="flex flex-col justify-between rounded-lg h-[220px] w-[480px] mx-auto mt-3 px-5 py-2">
                <form onSubmit={handleSubmit(submitDeleteRoom)} className="flex flex-col space-y-24" >
                    <div className="space-x-5">
                        <label className="text-gray-700 font-bold">
                            Are you sure to delete this room:
                        </label>
                        <label className="text-red-500 font-bold">
                            {room.name}
                        </label>
                    </div>
                    <div className="relative flex flex-col space-y-3">
                        <button
                            className="bg-orange-400 hover:bg-orange-600 text-white font-bold py-2 px-4 rounded-md focus:outline-none focus:shadow-outline"
                            type="submit"
                        >
                            Ok
                        </button>
                    </div>
                </form>
            </div>
        </div>
    )
}
