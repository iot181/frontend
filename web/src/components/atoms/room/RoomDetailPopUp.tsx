import {useContext, useEffect, useState} from "react";
import {Icons} from "../../../utils/icons/icons";
import DeviceDetailCard from "../device/DeviceDetailCard";
import DeviceListContainer from "../device/DeviceListContainer";
import EditRoomPopUp from "../device/EditRoomPopUp";
import NewDevicePopUp from "../device/NewDevicePopUp";
import DeleteDevicePopUp from "../device/DeleteDevicePopUp";
import {getDetail} from "../../../api/home/request";
import {toast} from "react-toastify";
import {AuthContext} from "../../../context/AuthContext";
import {getList} from "../../../api/device/request";

export default function RoomDetailPopUp(props) {
    const {token} = useContext(AuthContext)
    const {setOpenDetail, home, room, setEditRoomOpen  } = props;
    const [devices, setDevices] = useState([])
    const [openEdit, setOpenEdit] = useState(false);
    const [openAddDevice, setOpenAddDevice] = useState(false);
    const [openDeleteDevice, setOpenDeleteDevice] = useState(false);
    const [targetedDevice, setTargetedDevice] = useState({})
    const [seconds, setSeconds] = useState(0);

    useEffect(() => {
        const timeout = setTimeout(() => {
            (async () => {
                const res = await getList(token, 1,  100 , "", "", "", room._id)
                if (!res.success) {

                }
                else {
                    setDevices(res.data)
                }
            })()
            setSeconds(seconds + 5)
        }, 2500)
        return () => clearTimeout(timeout);
    }, [openAddDevice, openDeleteDevice, seconds])

    const exitEdit = () => {
        setOpenEdit(false);
        setOpenDetail(false);
        setEditRoomOpen(true);
    }
    const exitAddDevice = (exit: boolean) => {
        setOpenAddDevice(false);
        setOpenDetail(true);
        setEditRoomOpen(true);
    }
    const exitDeleteDevice = () => {
        setOpenDeleteDevice(false);
        setOpenDetail(false);
        setEditRoomOpen(true);
    }

    const exitPopUp = () => {
        setOpenDetail(false)
    }
    return (
        <div className= "fixed mx-auto my-auto inset-0 h-[550px] w-[1000px] border-2 bg-white shadow-gray-300 shadow-xl rounded-2xl">
            <div className="relative flex-1 bg-green-500 text-lg font-bold text-white rounded-t-xl text-center py-2">
                <div> {home.name.toUpperCase()} </div>
                <div className="absolute right-0 top-0 px-2 py-2">
                    <button className="rounded-full bg-red-400 hover:bg-red-600 ring-2 hover:ring-4 px-2 py-2 " onClick={exitPopUp}>
                        <svg height={15} width={15}>{Icons.icon.close}</svg>
                    </button>
                </div>
            </div>
            <div className="flex flex-row space-x-0 px-3 py-5 justify-between">
                <DeviceDetailCard roomData={room} devices={devices} setOpenEdit={setOpenEdit}/>
                <DeviceListContainer setOpenDetail = { setOpenDetail } setOpenAddDevice={setOpenAddDevice} devices={devices} setDeleteOpen={setOpenDeleteDevice} setTargetedDevice={setTargetedDevice}/>
            </div>
            {openEdit ? <EditRoomPopUp setOpenEdit={setOpenEdit} exitEdit={exitEdit} homeId={home._id} room={room}></EditRoomPopUp>: <></>}
            {openAddDevice ? <NewDevicePopUp setOpenAddDevice={setOpenAddDevice} setOpenDetail={setOpenDetail} exitAddDevice={exitAddDevice} homeId={home._id} roomId={room._id}></NewDevicePopUp>: <></>}
            {openDeleteDevice ? <DeleteDevicePopUp setDeleteOpen={setOpenDeleteDevice} device={targetedDevice}></DeleteDevicePopUp>: <></>}
        </div>
    )
}
