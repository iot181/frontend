import RoomCard from "./RoomCard";
import {useNavigate} from "react-router-dom";
import {Icons} from "../../../utils/icons/icons";
import {useContext, useEffect, useState} from "react";
import DeleteRoomPopUp from "./DeleteRoomPopUp";
import {getDetail, getList} from "../../../api/home/request";
import {toast} from "react-toastify";
import {AuthContext} from "../../../context/AuthContext";

export default function RoomListContainer(props: any) {
    const {token} = useContext(AuthContext)
    const {setOpenDetail, setRoom, setDeviceInRoom, setNewOpen, home, openDetail, deleteOpen, setDeleteOpen} = props;
    const [homeData, setHomeData] = useState(home)
    const navigate = useNavigate();
    const backToHomeList = () => {
        navigate('/');
    }

    useEffect(() => {
        (async () => {
            const res = await getDetail(token, home._id)
            if (!res.success) {

            }
            else {
                setHomeData(res.data);
            }
        })()
    }, [openDetail])

    const [targetedRoom, setTargetedRoom] = useState({})
    return (
        <div>
            <div className="flex flex-col px-10 py-2 space-y-5 h-[550px] w-[900px] border-l-2">
                <div className="flex flex-row items-center justify-between">
                    <div className="text-lg font-bold">Room List: </div>
                    <div className="flex flex-row justify-end space-x-2">
                        <button className="border-gray-200 border-2 px-3 py-1 rounded-lg font-semibold hover:bg-gray-100" onClick={backToHomeList}> Back </button>
                        <button className="flex flex-row bg-green-500 text-white px-3 py-1 rounded-lg font-semibold hover:bg-green-600 items-center" onClick={()=> setNewOpen(true)}>
                            <svg className="mr-1 w-4 h-4">{Icons.icon.add}</svg>
                            New room
                        </button>
                    </div>
                </div>
                <div className="h-[450px] overflow-y-auto flex-col space-y-7 px-5 py-7">
                    {(home.rooms) ? home.rooms.map((roomData: any, index: number) => {
                        return (<RoomCard setRoom={setRoom}
                                          setOpenDetail={setOpenDetail}
                                          openDetail={openDetail}
                                          roomData = {roomData}
                                          setDeviceInRoom={setDeviceInRoom}
                                          inListView= {true}
                                          homeOrderNumber = {index}
                                          key={index}
                                          setDeleteOpen={setDeleteOpen}
                                          setTargetedRoom={setTargetedRoom}

                        />)
                    }) : (
                        <div className="font-bold text-lg"> This home have no room. Add new room!</div>
                    )}
                </div>
            </div>
            {deleteOpen? <DeleteRoomPopUp setDeleteOpen={setDeleteOpen} homeId={home._id} room={targetedRoom}></DeleteRoomPopUp> : <></>}
        </div>
    )
}
