import {Images} from "../../../utils/images/images";
import {Icons} from "../../../utils/icons/icons";
import {useEffect, useState} from "react";
import {getWeather} from "../../../api/weather/request";

export default function HomeDetailCard(props: any) {
    const {home, setEditOpen} = props;
    const [weather, setWeather] = useState({
        main: {
            "temp": 300.15,
            "feels_like": 301.76,
            "temp_min": 300.15,
            "temp_max": 300.15,
            "pressure": 1019,
            "humidity": 67,
            "sea_level": 1019,
            "grnd_level": 1017
        },
        weather: [{
            main: '',
            description: "",
        }],
        wind: {
            "speed": 1.43,
            "deg": 317,
            "gust": 1.62
        }
    })
    const roomString = (home.rooms.length).toString() + ((home.rooms.length > 1)? ' rooms': ' room')

    useEffect(() => {
        (async () => {

            const roomAddress = home?.address? home.address: 'Hanoi';
            const todayWeather = await getWeather(roomAddress)
            setWeather(todayWeather)
        })()
    }, [])

    return (
        <div>
            <div className="relative flex flex-col rounded-lg mx-1 px-4 py-1 space-y-3 h-[530px]" onClick={() => {}}>
                <div className="flex flex-row justify-between items-center">
                    <div className="text-lg font-bold">{home.name}</div>
                    <div className="text-sm font-bold">{roomString} </div>
                </div>
                <div className="h-[300px] overflow-hidden rounded-lg">
                    { (!home.imagesUrl)  ? Images.homeImages[1 % (Images.homeImages.length)]
                        : <img className="object-fill h-full" src={home.imagesUrl} alt={home._id}/>}
                </div>
                <div className="flex flex-col space-y-2 border-0 border-t-2 py-2 border-gray-200 px-4 rounded-lg">
                    <div className='text-sm'> Weather: {weather.weather[0].main} ({weather.weather[0].description}) </div>
                    <div className="grid grid-cols-2 gap-x-10 gap-y-1">
                        <div className="col-span-2 text-xs"><b>Temperature:</b> {weather.main?.temp}℃ &nbsp; &nbsp; (min: {weather.main?.temp_min}℃ - max: {weather.main?.temp_max}℃) </div>
                        <div className="text-xs"><b>Air pressure:</b> {weather.main?.pressure}00 Pa</div>
                        <div className="text-xs"><b>Humidity:</b> {weather.main?.humidity}%</div>
                        <div className="text-xs"><b>Wind:</b> {weather.wind?.speed}km/h, {weather.wind?.deg}°</div>
                    </div>
                    <div className="font-bold text-lg"></div>
                </div>
                <button className="absolute flex flex-row justify-center items-center space-x-2 bottom-0 right-2 left-2 bg-blue-500 rounded-lg py-2 text-white font-bold hover:bg-blue-700" onClick={()=> setEditOpen(true)}>
                    <svg height={20} width={20}>{Icons.icon.edit}</svg>
                    <div>EDIT</div>
                </button>
            </div>
        </div>
    )
}