import {useContext, useEffect, useState} from "react";
import {Icons} from "../../../utils/icons/icons";
import {createRoom} from "../../../api/room/request";
import {AuthContext} from "../../../context/AuthContext";
import {toast} from "react-toastify";
import { useForm, Controller} from "react-hook-form";
import Select from 'react-select';

const typeOptions = [
    {value: 'Living room', label: 'Living room'},
    {value: 'Bathroom', label: 'Bathroom'},
    {value: 'Bedroom', label: 'Bedroom'},
    {value: 'Kitchen', label: 'Kitchen'},
]

export default function NewRoomPopUp(props: any) {
    const {token} = useContext(AuthContext)
    const {setNewOpen, homeId} = props;
    const { register, handleSubmit, formState: { errors }, control} = useForm({
        criteriaMode: "all"
    });

    const submitCreateNewRoom = async (name: any) => {
        const response = await createRoom(token, homeId, name)
        if (response.success) {
            setNewOpen(false);
            toast.success(response.message);
        } else {
            toast.error(response.message);
        }
    }
    return (
        <div className= "fixed mx-auto my-auto inset-0 h-[350px] w-[500px] border-2 bg-white shadow-gray-300 shadow-xl rounded-xl border-2 border-gray-400 z-index-2">
            <div className="relative flex-1 bg-green-500 text-lg font-bold text-white rounded-t-xl text-center py-2">
                <div> CREATE NEW ROOM </div>
                <div className="absolute right-0 top-0 px-2 py-2">
                    <button className="rounded-full bg-red-400 hover:bg-red-600 ring-2 hover:ring-4 px-2 py-2 " onClick={() => setNewOpen(false)}>
                        <svg height={15} width={15}>{Icons.icon.close}</svg>
                    </button>
                </div>
            </div>
            <div className="flex flex-col justify-between rounded-lg h-[320px] w-[480px] mx-auto mt-3 px-5 py-2">
                <form onSubmit={handleSubmit(submitCreateNewRoom)} className="flex flex-col space-y-12" >
                    <div className="flex flex-col">
                        <div className="space-y-1">
                            <label className="text-gray-700 font-bold">
                                New room's name:
                            </label>
                            <input
                                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                type="text" placeholder="Name of room" {...register("name", {required: true})}/>
                            {errors.name && <span className="text-red-500 text-sm"> ⚠ This field is required!</span>}
                        </div>
                        <div className="space-y-1">
                            <label className="text-gray-700 font-bold">
                                New room's type:
                            </label>
                            <div className='flex flex-row border-[1px] border-gray-200 rounded-lg outline-none items-center space-x-3'>
                                <Controller
                                    control={control}
                                    name='type'
                                    defaultValue=''
                                    render={({ field: { onChange, value, ref }}) => (
                                        <Select
                                            defaultValue={typeOptions[0]}
                                            value={typeOptions.find((c: any) => c.value === value)}
                                            onChange={val => onChange(val?.value)}
                                            options={typeOptions}
                                            className='w-full'
                                        />
                                    )}
                                />
                            </div>
                            {errors.type && <span className="text-red-500 text-sm"> ⚠ This field is required!</span>}
                        </div>
                    </div>
                    <div className="relative flex flex-col space-y-3">
                        <label className="text-gray-700 font-bold flex flex-row space-x-10">
                            New image:
                            <button
                                className="flex flex-row space-x-3 bg-gray-500 hover:bg-gray-700 text-white py-1 px-2 rounded-md focus:outline-none focus:shadow-outline ml-5"
                                type="submit">
                                <svg width={20} height={20}>{Icons.icon.upload}</svg>
                                Upload
                            </button>
                        </label>
                        <button
                            className="bg-orange-400 hover:bg-orange-600 text-white font-bold py-2 px-4 rounded-md focus:outline-none focus:shadow-outline"
                            type="submit"
                        >
                            Ok
                        </button>
                    </div>
                </form>
            </div>
        </div>
    )
}
