import {Images} from "../../../utils/images/images";

export default function WeatherCard(props: any) {
    const {weather} =  props
    const weatherMain = weather.weatherMain.split(", ")[0]

    return (
        <div className = "flex flex-col border-2 bg-white h-[470px] space-y-4 rounded-lg p-2 items-center">
            <div className="bg-white rounded-lg h-2/3 overflow-hidden">
                <div className='flex flex-row justify-center item-center py-10'>
                    {
                        (weatherMain === 'Clouds') ? Images.weatherImages.cloud :
                            (weatherMain === 'Clear') ? Images.weatherImages.clear :
                                (weatherMain === 'Rain') ? Images.weatherImages.rain :
                                    (weatherMain === 'Sunny') ? Images.weatherImages.sunny :
                                        Images.weatherImages.cold
                    }
                </div>
            </div>
            <div className="font-semibold ">
                {weather.weatherMain.toUpperCase()}
            </div>
            <div className="grid grid-cols-2 gap-x-10 gap-y-2">
                <div className="col-span-2 text-xs"><b>Temperature:</b> {weather.main?.temp}℃ &nbsp; &nbsp; (min: {weather.main?.temp_min}℃ - max: {weather.main?.temp_max}℃) </div>
                <div className="text-xs"><b>Air pressure:</b> {weather.main?.pressure}00 Pa</div>
                <div className="text-xs"><b>Humidity:</b> {weather.main?.humidity}%</div>
                <div className="text-xs"><b>Wind:</b> {weather.wind?.speed}km/h, {weather.wind?.deg}°</div>
            </div>
            <div className="font-bold text-lg">
                {weather.dateTime}
            </div>
        </div>
    )
}
