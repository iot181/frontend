import {Images} from "../../../utils/images/images";
import {useContext, useEffect, useState} from "react";
import {changeStatusDetail, getList} from "../../../api/device/request";
import {toast} from "react-toastify";
import {AuthContext} from "../../../context/AuthContext";
import DeviceItemList from "../device/DeviceItemList";
import {Icons} from "../../../utils/icons/icons";

export default function DeviceCard(props: any) {
    const {token} = useContext(AuthContext)
    const {device, setDeleteOpen, setTargetedDevice} = props;
    const [status, setStatus] = useState(device.control.status);
    const changeStatusOfDevice = async (status) => {
        const result = await changeStatusDetail(token, {_id: device._id, control: { status: status? "1": "0"}});
        if (!result?.success) toast.error("Change status unsuccessful!")
        else {
            toast.success("Change status successful!")
            setStatus(status);
        }
    }
    return (
        <div className = "relative shadow-lg border-2 bg-gray-50 h-[115px] rounded-lg flex flex-row px-2 py-2 hover:bg-gray-100 ">
            <div className="rounded-md flex flex-col h-[100px] items-center py-2 justify-between px-1">
                <div className="flex flex-row items-center space-x-1.5">
                    <svg height={40} width={40}>{status? (Icons.icon.lightBulbOn) : (Icons.icon.lightBulbOff)}</svg>
                    <div className="flex flex-col w-[115px]">
                        <div className="text-md font-bold">{(device.deviceName.length > 12) ? `${device.deviceName.slice(0,12)}...` : device.deviceName }</div>
                        <div className="text-sm">{device.deviceType}</div>
                    </div>
                </div>
                <label className="relative inline-flex items-center cursor-pointer">
                    <input type="checkbox" value="" className="sr-only peer" checked={status} onClick={() => changeStatusOfDevice(!status)}/>
                    <div className="w-20 h-5 bg-gray-200 peer-focus:outline-none ring-2 ring-gray-300 peer-checked:ring-green-300 rounded-md peer peer-checked:after:translate-x-full after:content-[''] after:absolute after:top-[1px] after:left-[1px] after:bg-white after:rounded-md after:h-4 after:w-10 after:transition-all peer-checked:bg-green-500 peer:rounded-md"></div>
                </label>
            </div>
            <div className="h-[100px] w-[300px] border-l-2 flex flex-col py-2 space-y-1">
                <div className="flex flex-row items-center justify-between px-5">
                    <div className="text-sm">{`Direction: ${device.control.direction}`}</div>
                    <div className="text-sm">{`Intensity: ${device.control.intensity}`}</div>
                </div>
                <div className="flex flex-row items-center justify-between px-5">
                    <div className="text-sm">{`Mode: ${device.control.mode}`}</div>
                    <div className="text-sm">{`Speed: ${device.control.speed}`}</div>
                </div>
            </div>
            <button className="absolute rounded-full bg-red-400 hover:bg-red-600 ring-2  top-0 right-0 px-1 py-1" onClick={()=> {setDeleteOpen(true); setTargetedDevice(device)}}>
                <svg height={12} width={12}>{Icons.icon.close}</svg>
            </button>
        </div>
    )
}
