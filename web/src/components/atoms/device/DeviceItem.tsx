import {HomeCardWrapper} from "../../../style/styled";
import {Images} from "../../../utils/images/images";
import {useState} from "react";
import {Icons} from "../../../utils/icons/icons";
export default function DeviceItem(props: any) {
    const {device} = props;
    const [status, setStatus] = useState(device.control.status);
    return (
        <div className="rounded-md flex flex-col h-[65px] items-center py-2 justify-between">
            <div className="flex flex-row items-center space-x-1.5">
                <svg height={22} width={22}>{device.deviceType !== 'Common' ? (Icons.icon.lightBulbOn) : status ? (Icons.icon.lightBulbOn) : (Icons.icon.lightBulbOff)}</svg>
                <div className="flex flex-col">
                    <div className="text-xs font-bold">{(device.deviceName.length > 7) ? `${device.deviceName.slice(0,7)}...` : device.deviceName }</div>
                    <div className="text-[9px]">{device.deviceType}</div>
                </div>
            </div>
        </div>
    )
}
