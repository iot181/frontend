import {Images} from "../../../utils/images/images";
import DeviceItemList from "./DeviceItemList";
import {Icons} from "../../../utils/icons/icons";

export default function DeviceDetailCard(props: any) {
    const {roomData, devices, setOpenEdit} = props;
    const deviceString = devices.length + ((devices.length > 1) ? ' devices' : ' device');
    return (
        <div>
            <div className="relative flex flex-col rounded-lg mx-1 px-4 py-1 space-y-3 w-[400px] h-[470px]" onClick={() => {}}>
                <div className="flex flex-col">
                    <div className="flex flex-row items-center justify-between">
                        <div className="text-lg font-bold underline">{roomData.name? roomData.name: "Unname"}</div>
                        <div className="font-bold">{roomData.type? roomData.type: "Bedroom"}</div>
                    </div>
                    <div className="text-sm">{deviceString}</div>
                </div>
                <div className="rounded-lg w-[360px] mx-auto">
                    <div className="overflow-hidden rounded-lg">
                        {Images.homeImages[1]}
                    </div>
                </div>
                {
                    devices.map((device: any) => {
                        return (
                            device.deviceType === 'Sensor' &&
                            <div className='flex flex-col px-3 '>
                                <div className='font-semibold'>
                                    {device.deviceName}
                                </div>
                                <div className='px-3 grid grid-cols-2 gap-3'>
                                    {
                                        device.data.map((e: any) => {
                                            return (
                                                <div className=" px-5 py-2">
                                                    <div className="text-sm capitalize">{`${e.name}: ${e.value || ''}`}</div>
                                                </div>
                                            )
                                        })
                                    }
                                </div>
                            </div>
                        )
                    })
                }
                <button className="absolute flex flex-row justify-center items-center space-x-2 bottom-0 right-2 left-2 bg-blue-500 rounded-lg py-2 text-white font-bold hover:bg-blue-700" onClick={() => setOpenEdit(true)}>
                    <svg height={20} width={20}>{Icons.icon.edit}</svg>
                    <div >EDIT</div>
                </button>
            </div>
        </div>
    )
}