import {Images} from "../../../utils/images/images";
import {useContext, useEffect, useState} from "react";
import {changeStatusDetail, getList} from "../../../api/device/request";
import {toast} from "react-toastify";
import {AuthContext} from "../../../context/AuthContext";
import DeviceItemList from "../device/DeviceItemList";
import {Icons} from "../../../utils/icons/icons";

export default function SensorCard(props: any) {
    const {token} = useContext(AuthContext)
    const {device} = props;

    return (
        <div className = "relative shadow-lg border-2 bg-gray-50 h-[115px] rounded-lg flex flex-row px-2 py-2 hover:bg-gray-100 ">
            <div className="rounded-md flex flex-col h-[100px] items-center py-2 justify-between px-1">
                <div className="flex flex-row items-center space-x-1.5">
                    <svg height={40} width={40}>{Icons.icon.sensor}</svg>
                    <div className="flex flex-col w-[115px]">
                        <div className="text-md font-bold">{device.deviceName}</div>
                        <div className="text-sm">{device.deviceType}</div>
                    </div>
                </div>
            </div>
            <div className='border-l-2 grid grid-cols-2 gap-3'>
                {
                    device.data.map((e: any) => {
                        return (
                            <div className=" px-5 py-2">
                                <div className="text-sm capitalize">{`${e.name}: ${e.value || ''}`}</div>
                            </div>
                        )
                    })
                }
            </div>
        </div>
    )
}
