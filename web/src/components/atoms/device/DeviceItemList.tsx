import {HomeCardWrapper} from "../../../style/styled";
import {Images} from "../../../utils/images/images";
import DeviceItem from "./DeviceItem";
import SensorItem from "./SensorItem";

export default function DeviceItemList(props: any) {
    const {devices} = props;
    return (
        <div className='pb-12 h-[80px] flex flex-row border-t-2 border-gray-400 overflow-x-auto overflow-y-hidden'>
            <div className="flex flex-row h-[40px] space-x-2 ">
                {devices.map((device: any) => {
                    return (
                        device.deviceType === 'Common' &&
                            <div className='w-[90px]'>
                                <DeviceItem device={device} key={device._id}></DeviceItem>
                            </div>
                    )
                })}
                {devices.map((device: any) => {
                    return (
                        device.deviceType === 'Sensor' &&
                            <div className='w-200px'>
                                <SensorItem device={device} key={device._id}></SensorItem>
                            </div>
                    )
                })}
            </div>
        </div>
    )
}
