import {useContext, useEffect, useState} from "react";
import {Icons} from "../../../utils/icons/icons";
import {AuthContext} from "../../../context/AuthContext";
import {toast} from "react-toastify";
import { useForm } from "react-hook-form";
import {createDevice} from "../../../api/device/request";
import {createRoom} from "../../../api/room/request";

export default function NewDevicePopUp(props: any) {
    const {token} = useContext(AuthContext)
    const {setOpenAddDevice, homeId, roomId, exitAddDevice, setOpenDetail} = props;
    const { register, handleSubmit, formState: { errors } } = useForm({
        criteriaMode: "all"
    });

    const submitCreateNewDevice = async (body: any) => {
        const newDevice = {
            deviceName: body.deviceName,
            deviceType:  body.deviceType,
            control:
                {
                    mode: body.mode,
                    speed: body.speed,
                    status: body.status,
                    intensity: body.intensity,
                    direction: body.direction
                }
        }
        const response = await createDevice(token, homeId, roomId, newDevice)
        if (response.success) {
            exitAddDevice(true);
            toast.success(response.message);
        } else {
            toast.error(response.message);
        }
    }
    return (
        <div className= "fixed mx-auto my-auto inset-0 h-[600px] w-[550px] border-2 bg-white shadow-gray-300 shadow-xl rounded-xl border-2 border-gray-400 z-index-2">
            <div className="relative flex-1 bg-green-500 text-lg font-bold text-white rounded-t-xl text-center py-2">
                <div> CREATE NEW DEVICE </div>
                <div className="absolute right-0 top-0 px-2 py-2">
                    <button className="rounded-full bg-red-400 hover:bg-red-600 ring-2 hover:ring-4 px-2 py-2 " onClick={() => setOpenAddDevice(false)}>
                        <svg height={15} width={15}>{Icons.icon.close}</svg>
                    </button>
                </div>
            </div>
            <div className="flex flex-col justify-between rounded-lg h-[550px] w-[530px] mx-auto mt-3 px-5 py-2">
                <form onSubmit={handleSubmit(submitCreateNewDevice)} className="flex flex-col space-y-24" >
                    <div className="flex flex-col space-y-1">
                        <div className="flex flex-row items-center ">
                            <label className="text-gray-700 font-bold w-1/5"> Name: </label>
                            <input
                                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                type="text" placeholder="Name of device" {...register("deviceName", {required: true})}/>
                            {errors.name && <span className="text-red-500 text-sm"> ⚠ This field is required!</span>}
                        </div>
                        <div className="flex flex-row items-center ">
                            <label className="text-gray-700 font-bold w-1/5"> Type: </label>
                            <input
                                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                type="text" placeholder="Type of device" {...register("deviceType", {required: true})}/>
                            {errors.name && <span className="text-red-500 text-sm"> ⚠ This field is required!</span>}
                        </div>
                        <label className="text-gray-700 font-bold w-1/5 pt-5"> Control: </label>
                        <div className="flex flex-row items-center ml-8">
                            <label className="text-gray-700 font-semibold w-1/4"> Status: </label>
                            <input
                                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                type="text" placeholder="Default status" {...register("status", {required: true})}/>
                            {errors.name && <span className="text-red-500 text-sm"> ⚠ This field is required!</span>}
                        </div>
                        <div className="flex flex-row items-center ml-8">
                            <label className="text-gray-700 font-semibold w-1/4"> Mode: </label>
                            <input
                                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                type="text" placeholder="Default mode" {...register("mode", {required: true})}/>
                            {errors.name && <span className="text-red-500 text-sm"> ⚠ This field is required!</span>}
                        </div>
                        <div className="flex flex-row items-center ml-8 ">
                            <label className="text-gray-700 font-semibold w-1/4"> Direction: </label>
                            <input
                                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                type="text" placeholder="" {...register("direction", {required: true})}/>
                            {errors.name && <span className="text-red-500 text-sm"> ⚠ This field is required!</span>}
                        </div>
                        <div className="flex flex-row items-center ml-8">
                            <label className="text-gray-700 font-semibold w-1/4"> Speed: </label>
                            <input
                                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                type="text" placeholder="" {...register("speed", {required: true})}/>
                            {errors.name && <span className="text-red-500 text-sm"> ⚠ This field is required!</span>}
                        </div>
                        <div className="flex flex-row items-center ml-8">
                            <label className="text-gray-700 font-semibold w-1/4"> Intensity: </label>
                            <input
                                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                type="text" placeholder="Type of device" {...register("intensity", {required: true})}/>
                            {errors.name && <span className="text-red-500 text-sm"> ⚠ This field is required!</span>}
                        </div>
                    </div>
                    <div className="relative flex flex-col space-y-3">
                        <label className="text-gray-700 font-bold flex flex-row space-x-10">
                            New image:
                            <button
                                className="flex flex-row space-x-3 bg-gray-500 hover:bg-gray-700 text-white py-1 px-2 rounded-md focus:outline-none focus:shadow-outline ml-5"
                                type="submit">
                                <svg width={20} height={20}>{Icons.icon.upload}</svg>
                                Upload
                            </button>
                        </label>
                        <button
                            className="bg-orange-400 hover:bg-orange-600 text-white font-bold py-2 px-4 rounded-md focus:outline-none focus:shadow-outline"
                            type="submit"
                        >
                            Ok
                        </button>
                    </div>
                </form>
            </div>
        </div>
    )
}
