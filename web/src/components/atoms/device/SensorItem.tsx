import {useState} from "react";
import {Icons} from "../../../utils/icons/icons";
export default function SensorItem(props: any) {
    const {device} = props;
    return (
        <div className="rounded-md flex flex-col min-w-[120px] h-[65px] items-center py-2 justify-between">
            <div className="flex flex-row items-center space-x-1.5">
                <svg height={26} width={26}>{Icons.icon.sensor}</svg>
                <div className="flex flex-col">
                    <div className="text-xs font-bold">{(device.deviceName.length > 15) ? `${device.deviceName.slice(0,15)}` : device.deviceName }</div>
                    <div className="text-[9px]">{device.deviceType}</div>
                </div>
            </div>
        </div>
    )
}
