import DeviceCard from "./DeviceCard";
import {Icons} from "../../../utils/icons/icons";
import SensorCard from "./SensorCard";

export default function DeviceListContainer(props: any) {
    const {setOpenAddDevice, devices, setDeleteOpen, setTargetedDevice, setOpenDetail} = props;
    return (
        <div className="flex flex-col px-3 py-1 space-y-5 h-[460px] w-[520px] border-l-2">
            <div className="flex flex-row items-center justify-between">
                <div className="text-lg font-bold">Device List: </div>
                <button className="flex flex-row bg-green-500 text-white px-3 py-1 rounded-lg font-semibold hover:bg-green-600 items-center" onClick={()=> setOpenAddDevice(true)}>
                    <svg className="mr-1 w-4 h-4">{Icons.icon.add}</svg>
                    New device
                </button>
            </div>
            <div className="w-[520px] overflow-y-auto flex-col space-y-5 px-5">
                {(devices.length) ? devices.map((device: any, index: number) => {
                    return (device.deviceType === 'Common' ?
                        <DeviceCard device={device} setDeleteOpen={setDeleteOpen} setTargetedDevice={setTargetedDevice}/>
                            : <SensorCard device={device}/>
                    )
                }) : (
                    <div className="text-lg"> This room have no device. Add new device!</div>
                )}
            </div>
        </div>
    )
}
