import axios from 'axios';
import {IChangeStatusDetail, ICreateDevice, IChangeDeviceValue} from "./types";
const baseBeURL = `${process.env.REACT_APP_BE_HOST}/`

export const createDevice = async (token: string | null, homeId: string, roomId: string, data: ICreateDevice) => {
    try {
        const result = await axios.post(`${baseBeURL}home/room/device/create?homeId=${homeId}&&roomId=${roomId}`,data, {
            headers: {
                accept: 'application/json',
                authorization: `Bearer ${token}`,
                'content-type': 'application/json'
            }
        });
        return {
            success: result.data.success,
            data: result.data.data,
            message: 'Create new device successful!'
        };
    } catch (error: unknown) {
        if (axios.isAxiosError(error)) {
            if (error?.response?.status === 401) {
                return {
                    success: false,
                    data: null,
                    message: 'Unauthorized'
                };
            } else {
                return {
                    success: false,
                    data: null,
                    message: 'Failed to create data!'
                };
            }
        } else {
            return {
                success: false,
                message: 'Network error',
                data: null
            };
        }
    }
}

export const getList = async (token: string | null, page: number, limit: number, name: string, type: string, status: string, roomId: string) => {
    try {
        const result = await axios.get(`${baseBeURL}home/room/device/list?page=${page}&limit=${limit}&type=${type}&status=${status}&roomId=${roomId}&name=${name}`, {
            headers: {
                accept: 'application/json',
                authorization: `Bearer ${token}`,
                'content-type': 'application/json'
            }
        });
        return {
            success: result.data.success,
            data: result.data.data,
            pagination: result.data.pagination,
            message: 'Get list device successful!'
        };
    } catch (error: unknown) {
        if (axios.isAxiosError(error)) {
            if (error?.response?.status === 401) {
                return {
                    success: false,
                    data: null,
                    message: 'Unauthorized'
                };
            } else {
                return {
                    success: false,
                    data: null,
                    message: 'Failed to get data!'
                };
            }
        } else {
            return {
                success: false,
                message: 'Network error',
                data: null
            };
        }
    }
}

export const deleteDetail = async (token: string | null, id: string) => {
    try {
        const result = await axios.delete(`${baseBeURL}home/room/device/${id}`, {
            headers: {
                accept: 'application/json',
                authorization: `Bearer ${token}`,
                'content-type': 'application/json'
            }
        });
        return {
            success: result.data.success,
            data: result.data.data,
            message: 'Delete device successful!'
        };
    } catch (error: unknown) {
        if (axios.isAxiosError(error)) {
            if (error?.response?.status === 401) {
                return {
                    success: false,
                    data: null,
                    message: 'Unauthorized'
                };
            } else {
                return {
                    success: false,
                    data: null,
                    message: 'Failed to delete data!'
                };
            }
        } else {
            return {
                success: false,
                message: 'Network error',
                data: null
            };
        }
    }
}

export const changeStatusDetail = async (token: string | null, props: IChangeDeviceValue) => {
    try {
        const result = await axios.put(`${baseBeURL}home/room/device/${props._id}`,props,{
            headers: {
                accept: 'application/json',
                authorization: `Bearer ${token}`,
                'content-type': 'application/json'
            }
        })
        return {
            success: true,
            data: result.data.data,
            message: 'Change status successfully!'
        };
    } catch (error: unknown) {
        console.log("ERROR", error)
    }
}