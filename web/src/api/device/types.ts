export interface ICreateDevice {
    deviceName: string,
    deviceType: string,
    control: IControl,
}

interface IControl {
    status: string,
    mode: string,
    direction: string,
    speed: string,
    intensity: string
}

export interface IChangeStatusDetail {
    status: boolean
}

interface IControlDeviceValue {
    status: string
}

export interface IChangeDeviceValue {
    _id: string,
    control: IControlDeviceValue
}
