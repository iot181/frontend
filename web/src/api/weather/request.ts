import axios from 'axios';

const API_KEY = '1720abd99832e0a1fa3d91db825e8482';

export async function getWeather(city) {
    const url = `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${API_KEY}`;
    const response = await axios.get(url);
    const data = response.data;
    return data
}


export async function getFiveDayWeather(city) {
    const url = `https://api.openweathermap.org/data/2.5/forecast?appid=${API_KEY}&q=${city}&units=metric`;
    const response = await axios.get(url);
    const data = response.data.list;
    const result: any[] =[], dateArray: any[] = [];
    data.map((e: any) => {
        const dateString = new Date(e.dt_txt).toLocaleDateString()
        if (!dateArray.includes(dateString)) {
            dateArray.push(dateString)
            result.push({dateTime: dateString, data: []})
        }
        result.map((datagram: any) => {
            if (datagram.dateTime === dateString) datagram.data.push({
                time: new Date(e.dt_txt).toLocaleTimeString(),
                weather: e.weather[0],
                wind: e.wind,
                main: e.main
            })
        })
    })

    result.map((e: any) => {
        let wind = {speed: 0, deg: 0, gust: 0}
        let main = {humidity: 0, pressure: 0, sea_level: 0, temp: 0, temp_max: 0, temp_min: e.data[0].main.temp_min}
        let weatherMain : any [] = [];

        e.data.map((hourData: any) => {
            wind.speed += hourData.wind.speed
            wind.deg += hourData.wind.deg
            wind.gust += hourData.wind.gust

            main.humidity += hourData.main.humidity
            main.pressure += hourData.main.pressure
            main.sea_level += hourData.main.sea_level
            main.temp += hourData.main.temp
            main.temp_max = hourData.main.temp_max > main.temp_max? hourData.main.temp_max: main.temp_max
            main.temp_min = hourData.main.temp_min < main.temp_min? hourData.main.temp_min: main.temp_min

            if (!weatherMain.includes(hourData.weather.main)) weatherMain.push(hourData.weather.main)

        })
        wind.speed = Number((wind.speed / e.data.length).toFixed(2))
        wind.deg = parseInt(String(wind.deg / e.data.length))
        wind.gust = Number((wind.gust / e.data.length).toFixed(2))

        main.humidity = parseInt(String(main.humidity / e.data.length))
        main.pressure = parseInt(String(main.pressure / e.data.length))
        main.sea_level = parseInt(String(main.sea_level / e.data.length))
        main.temp = parseInt(String(main.temp / e.data.length))

        e.wind = wind;
        e.main = main;
        e.weatherMain = weatherMain.join(", ")

    })
    return result
}
