import axios from 'axios';
import { ILoginRequest, IRegisterRequest, IEditPassword, IEditProfile } from './types';
const baseBeURL = `${process.env.REACT_APP_BE_HOST}`

export const login = async (props: ILoginRequest) => {
    try {
        const result = await axios.post(`${baseBeURL}/user/login`, props)
        return {
            success: true,
            data: result.data.data,
            message: 'Login successfully!'
        };
    } catch (error: unknown) {
        if (axios.isAxiosError(error)) {
            return {
                success: false,
                data: null,
                message: "Invalid username or password"
            };
        } else {
            return {
                success: false,
                message: 'Network error',
                data: null
            };
        }
    }
};

export const getProfile = async(token) => {
    try {
        const result = await axios.get(`${baseBeURL}/user/info`, {
            headers: {
                accept: 'application/json',
                authorization: `Bearer ${token}`,
                'content-type': 'application/json'
            }
        });
        return {
            success: true,
            data: result.data.data,
            message: 'Get user info successfully!'
        };
    } catch (error: unknown) {
        return {
            success: false,
            data: null,
            message: 'You need to login first!'
        };
    }
}

export const register = async (props: IRegisterRequest) => {
    try {
        const result = await axios.post(`${baseBeURL}/user/register`, props)
        return {
            success: true,
            data: result.data.data,
            message: 'Register successfully!'
        };
    } catch (error: unknown) {
        if (axios.isAxiosError(error)) {
            return {
                success: false,
                data: null,
                message: error?.message
            };
        } else {
            return {
                success: false,
                message: 'Network error',
                data: null
            };
        }
    }
};

export const updatePassword = async (token: string | null, props: IEditPassword) => {
    try {
        const result = await axios.post(`${baseBeURL}/user/update-password`, props, {
            headers: {
                accept: 'application/json',
                authorization: `Bearer ${token}`,
                'content-type': 'application/json'
            }
        })
        return {
            success: true,
            data: result.data.data,
            message: 'Update password successfully!'
        };
    } catch (error: unknown) {
        if (axios.isAxiosError(error)) {
            return {
                success: false,
                data: null,
                message: error?.message
            };
        } else {
            return {
                success: false,
                message: 'Network error',
                data: null
            };
        }
    }
}

export const updateProfile = async (token: string | null, props: IEditProfile) => {
    try {
        const result = await axios.post(`${baseBeURL}/user/update-info`, props, {
            headers: {
                accept: 'application/json',
                authorization: `Bearer ${token}`,
                'content-type': 'application/json'
            }
        })
        return {
            success: true,
            data: result.data.data,
            message: 'Update profile successfully!'
        };
    } catch (error: unknown) {
        if (axios.isAxiosError(error)) {
            return {
                success: false,
                data: null,
                message: error?.message
            };
        } else {
            return {
                success: false,
                message: 'Network error',
                data: null
            };
        }
    }
}


