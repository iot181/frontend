export interface ICreateRoom {
    name: string
}

export interface IUpdateRoom {
    name: string
}