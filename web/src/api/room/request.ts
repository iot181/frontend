import axios from 'axios';
import {ICreateRoom, IUpdateRoom} from "./types";

const baseBeURL = `${process.env.REACT_APP_BE_HOST}/`

export const createRoom = async (token: string | null,homeId: string,  data: ICreateRoom) => {
    try {
        const result = await axios.post(`${baseBeURL}home/room/create?homeId=${homeId}`,data, {
            headers: {
                accept: 'application/json',
                authorization: `Bearer ${token}`,
                'content-type': 'application/json'
            }
        });
        return {
            success: result.data.success,
            data: result.data.data,
            message: 'Create new room successful!'
        };
    } catch (error: unknown) {
        if (axios.isAxiosError(error)) {
            if (error?.response?.status === 401) {
                return {
                    success: false,
                    data: null,
                    message: 'Unauthorized'
                };
            } else {
                return {
                    success: false,
                    data: null,
                    message: 'Failed to create data!'
                };
            }
        } else {
            return {
                success: false,
                message: 'Network error',
                data: null
            };
        }
    }
}

export const getList = async (token: string | null, page: number, limit: number, homeId: string,) => {
    try {
        const result = await fetch(`${baseBeURL}home/room/list?page=${page}&limit=${limit}&homeId=${homeId}`, {
            method: 'GET',
            headers: {
                accept: 'application/json',
                authorization: `Bearer ${token}`,
                'content-type': 'application/json/x-www-form-urlencoded'
            }
        });
        return result.json()
    } catch (error: unknown) {
        if (axios.isAxiosError(error)) {
            if (error?.response?.status === 401) {
                return {
                    success: false,
                    data: null,
                    message: 'Unauthorized'
                };
            } else {
                return {
                    success: false,
                    data: null,
                    message: 'Failed to get data!'
                };
            }
        } else {
            return {
                success: false,
                message: error,
                data: null
            };
        }
    }
}

export const getDetail = async (token: string | null, id: string) => {
    try {
        const result = await axios.get(`${baseBeURL}home/room/${id}`, {
            headers: {
                accept: 'application/json',
                authorization: `Bearer ${token}`,
                'content-type': 'application/json'
            }
        });
        return {
            success: result.data.success,
            data: result.data.data,
            message: 'Get room detail successful!'
        };
    } catch (error: unknown) {
        if (axios.isAxiosError(error)) {
            if (error?.response?.status === 401) {
                return {
                    success: false,
                    data: null,
                    message: 'Unauthorized'
                };
            } else {
                return {
                    success: false,
                    data: null,
                    message: 'Failed to get data!'
                };
            }
        } else {
            return {
                success: false,
                message: 'Network error',
                data: null
            };
        }
    }
}

export const updateDetail = async (token: string | null, id: string, updateBody: IUpdateRoom) => {
    try {
        const result = await axios.put(`${baseBeURL}home/room/${id}`,updateBody, {
            headers: {
                accept: 'application/json',
                authorization: `Bearer ${token}`,
                'content-type': 'application/json'
            }
        });
        return {
            success: result.data.success,
            data: result.data.data,
            message: 'Update room detail successful!'
        };
    } catch (error: unknown) {
        if (axios.isAxiosError(error)) {
            if (error?.response?.status === 401) {
                return {
                    success: false,
                    data: null,
                    message: 'Unauthorized'
                };
            } else {
                return {
                    success: false,
                    data: null,
                    message: 'Failed to update data!'
                };
            }
        } else {
            return {
                success: false,
                message: 'Network error',
                data: null
            };
        }
    }
}

export const deleteDetail = async (token: string | null, id: string) => {
    try {
        const result = await axios.delete(`${baseBeURL}home/room/${id}`, {
            headers: {
                accept: 'application/json',
                authorization: `Bearer ${token}`,
                'content-type': 'application/json'
            }
        });
        return {
            success: result.data.success,
            data: result.data.data,
            message: 'Delete room successful!'
        };
    } catch (error: unknown) {
        if (axios.isAxiosError(error)) {
            if (error?.response?.status === 401) {
                return {
                    success: false,
                    data: null,
                    message: 'Unauthorized'
                };
            } else {
                return {
                    success: false,
                    data: null,
                    message: 'Failed to delete data!'
                };
            }
        } else {
            return {
                success: false,
                message: 'Network error',
                data: null
            };
        }
    }
}

