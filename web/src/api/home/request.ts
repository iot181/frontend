import axios from 'axios';
import {ICreateHome, IUpdateHome} from "./types";
const baseBeURL = `${process.env.REACT_APP_BE_HOST}/`

export const createHome = async (token: string | null, data: ICreateHome) => {
    try {
        const result = await axios.post(`${baseBeURL}home/create`,data, {
            headers: {
                accept: 'application/json',
                authorization: `Bearer ${token}`,
                'content-type': 'application/json'
            }
        });
        return {
            success: result.data.success,
            data: result.data.data,
            message: 'Create new home successful!'
        };
    } catch (error: unknown) {
        if (axios.isAxiosError(error)) {
            if (error?.response?.status === 401) {
                return {
                    success: false,
                    data: null,
                    message: 'Unauthorized'
                };
            } else {
                return {
                    success: false,
                    data: null,
                    message: 'Failed to create data!'
                };
            }
        } else {
            return {
                success: false,
                message: 'Network error',
                data: null
            };
        }
    }
}

export const getList = async (token: string | null, page: number, limit: number, name: string,) => {
    try {
        const result = await fetch(`${baseBeURL}home/list?page=${page}&limit=${limit}&name=${name}`, {
            method: 'GET',
            headers: {
                accept: 'application/json',
                authorization: `Bearer ${token}`,
                'content-type': 'application/json/x-www-form-urlencoded'
            }
        });
        const response = result.json();
        return response
    } catch (error: unknown) {
        if (axios.isAxiosError(error)) {
            if (error?.response?.status === 401) {
                return {
                    success: false,
                    data: null,
                    message: 'Unauthorized'
                };
            } else {
                return {
                    success: false,
                    data: null,
                    message: 'Failed to get data!'
                };
            }
        } else {
            return {
                success: false,
                message: error,
                data: null
            };
        }
    }
}

export const getDetail = async (token: string | null, id: string) => {
    try {
        const result = await axios.get(`${baseBeURL}home/${id}`, {
            headers: {
                accept: 'application/json',
                authorization: `Bearer ${token}`,
                'content-type': 'application/json'
            }
        });
        return {
            success: result.data.success,
            data: result.data.data,
            message: 'Get home detail successful!'
        };
    } catch (error: unknown) {
        if (axios.isAxiosError(error)) {
            if (error?.response?.status === 401) {
                return {
                    success: false,
                    data: null,
                    message: 'Unauthorized'
                };
            } else {
                return {
                    success: false,
                    data: null,
                    message: 'Failed to get data!'
                };
            }
        } else {
            return {
                success: false,
                message: 'Network error',
                data: null
            };
        }
    }
}

export const updateDetail = async (token: string | null, id: string, updateBody: IUpdateHome) => {
    try {
        const result = await axios.put(`${baseBeURL}home/${id}`,updateBody, {
            headers: {
                accept: 'application/json',
                authorization: `Bearer ${token}`,
                'content-type': 'application/json'
            }
        });
        return {
            success: result.data.success,
            data: result.data.data,
            message: 'Update home detail successful!'
        };
    } catch (error: unknown) {
        if (axios.isAxiosError(error)) {
            if (error?.response?.status === 401) {
                return {
                    success: false,
                    data: null,
                    message: 'Unauthorized'
                };
            } else {
                return {
                    success: false,
                    data: null,
                    message: 'Failed to update data!'
                };
            }
        } else {
            return {
                success: false,
                message: 'Network error',
                data: null
            };
        }
    }
}

export const deleteDetail = async (token: string | null, id: string) => {
    try {
        const result = await axios.delete(`${baseBeURL}home/${id}`, {
            headers: {
                accept: 'application/json',
                authorization: `Bearer ${token}`,
                'content-type': 'application/json'
            }
        });
        return {
            success: result.data.success,
            data: result.data.data,
            message: 'Delete home successful!'
        };
    } catch (error: unknown) {
        if (axios.isAxiosError(error)) {
            if (error?.response?.status === 401) {
                return {
                    success: false,
                    data: null,
                    message: 'Unauthorized'
                };
            } else {
                return {
                    success: false,
                    data: null,
                    message: 'Failed to delete data!'
                };
            }
        } else {
            return {
                success: false,
                message: 'Network error',
                data: null
            };
        }
    }
}