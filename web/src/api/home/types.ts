export interface ICreateHome {
    name: string
}

export interface IUpdateHome {
    name: string
}