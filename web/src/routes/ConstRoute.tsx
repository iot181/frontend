import {Icons} from "../utils/icons/icons";
import Notification from "../components/pages/Notification";
import Home from "../components/pages/Home";
import Status from "../components/pages/Status";
import Weather from "../components/pages/Weather";
import Profile from "../components/pages/Profile";
import Room from "../components/pages/Room";

export const constRoute = [
    {
        title: 'Home',
        path: '/',
        icon: Icons.icon.home,
        element: <Home/>,
        children: [
            {
                path: '/:id',
                element: <Room/>
            }
        ]
    },
    {
        title: 'Notification',
        path: '/notification',
        icon: Icons.icon.notification,
        element: <Notification/>
    },
    {
        title: 'Status',
        path: '/status',
        icon: Icons.icon.status,
        element: <Status/>
    },
    {
        title: 'Weather',
        path: '/weather',
        icon: Icons.icon.weather,
        element: <Weather/>
    },
    {
        title: 'Profile' ,
        path: '/profile',
        icon: Icons.icon.personal,
        element: <Profile/>,
    }
]