import Layout from "../components/templates/Layout";
import LoginPage from "../components/pages/Login";
import {constRoute} from "./ConstRoute";
import RegisterPage from "../components/pages/Register";

let mainLayoutChildren : any [] =[];
constRoute.map(e => {
    mainLayoutChildren.push({path: e.path, element: e.element})
    if (e?.children !== undefined) {
        e?.children?.map((child => {
            mainLayoutChildren.push({path: child.path, element: child.element})
        }))
    }
})

export const routes = [
    {
        path: '/',
        element: <Layout/>,
        children: mainLayoutChildren
    },
    {
        path: '/login',
        element: <LoginPage/>
    },
    {
        path: '/register',
        element: <RegisterPage/>
    }
];
